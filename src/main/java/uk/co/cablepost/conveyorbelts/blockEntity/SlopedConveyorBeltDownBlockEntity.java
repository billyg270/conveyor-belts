package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.block.ConveyorBelt;
import uk.co.cablepost.conveyorbelts.block.SlopedConveyorBeltUp;

import java.util.Objects;

public class SlopedConveyorBeltDownBlockEntity extends SlopedConveyorBeltUpBlockEntity {

    public SlopedConveyorBeltDownBlockEntity(BlockPos pos, BlockState state, BlockEntityType blockEntityType) {
        super(pos, state, blockEntityType);
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, SlopedConveyorBeltDownBlockEntity blockEntity) {

        boolean top = state.get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER;

        if(!blockEntity.copiedItemsFromBelowIfTop && top) {
            try {
                blockEntity.items = ((SlopedConveyorBeltDownBlockEntity) Objects.requireNonNull(world.getBlockEntity(pos.add(0, -1, 0)))).items;
            }
            catch(java.lang.NullPointerException e){
                //other half of sloped conveyor is not below
                blockEntity.copiedItemsFromBelowIfTop = true;//just do this to stop it trying over and over
                return;
            }
            blockEntity.copiedItemsFromBelowIfTop = true;
        }

        if(!state.get(ConveyorBelt.ENABLED) || top){
            blockEntity.updateSlotActuallyEmptyHack();
            return;
        }

        for(int i = 0; i < 3; i++) {
            if(blockEntity.getStack(i).isEmpty()){
                //if empty slot, reset cool-downs
                blockEntity.resetCooldowns(i);
                continue;
            }

            blockEntity.updateCooldowns(i);

            //if cool-down has run down, meaning item can goto next slot
            if (blockEntity.transferCooldownCounter[i] <= 0) {
                blockEntity.transferCooldownCounter[i] = 0;

                if(i == 2){
                    //if last slot in belt, will goto next belt
                    Direction direction = state.get(ConveyorBelt.FACING);
                    blockEntity.moveToNextBeltSloped(direction, i, false);
                }
                else {
                    //else, moving from one slot to another within this belt

                    if(transferToEmpty(blockEntity, i, blockEntity, i + 1)){
                        blockEntity.transferCooldownCounter[i + 1] = blockEntity.transferCooldown;
                        blockEntity.transferSidewaysOffset[i + 1] = blockEntity.transferSidewaysOffset[i];
                        blockEntity.markDirty();
                    }
                }
            }
        }

        blockEntity.updateSlotActuallyEmptyHack();
    }
}
