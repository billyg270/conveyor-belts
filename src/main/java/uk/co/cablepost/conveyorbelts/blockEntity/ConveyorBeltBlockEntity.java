package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.block.ConveyorBelt;
import uk.co.cablepost.conveyorbelts.block.SlopedConveyorBeltUp;
import uk.co.cablepost.conveyorbelts.screenHandler.ConveyorBeltScreenHandler;

import java.util.Objects;

public class ConveyorBeltBlockEntity extends BlockEntity implements NamedScreenHandlerFactory, SidedInventory, Inventory {

    public DefaultedList<ItemStack> items = DefaultedList.ofSize(3, ItemStack.EMPTY);

    public int transferCooldown = 30;
    public int moveToCenterSpeed = 2;
    public int[] transferCooldownCounter = new int[size()];
    public int[] transferCooldownCounterLastTick = new int[10];
    public int[] transferSidewaysOffset = new int[size()];
    public int[] slotActuallyHasItem = new int[size()];

    public int[] sideTransferAttempts = new int[2];
    public long[] sideTransferLatestAttempt = new long[3];

    public ConveyorBeltBlockEntity(BlockPos pos, BlockState state, BlockEntityType blockEntityType) {
        super(blockEntityType, pos, state);
    }

    @Override
    public void writeNbt(NbtCompound tag, RegistryWrapper.WrapperLookup wrapperLookup) {

        Inventories.writeNbt(tag, items, wrapperLookup);
        tag.putIntArray("TransferCooldownCounter", transferCooldownCounter);
        tag.putIntArray("TransferSidewaysOffset", transferSidewaysOffset);
        tag.putIntArray("SlotActuallyHasItem", slotActuallyHasItem);

        tag.putIntArray("SideTransferAttempts", sideTransferAttempts);
        tag.putLongArray("SideTransferLatestAttempt", sideTransferLatestAttempt);

        super.writeNbt(tag, wrapperLookup);

        //return tag;
    }

    @Override
    public void readNbt(NbtCompound tag, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(tag, wrapperLookup);

        Inventories.readNbt(tag, items, wrapperLookup);
        transferCooldownCounter = tag.getIntArray("TransferCooldownCounter");
        transferSidewaysOffset = tag.getIntArray("TransferSidewaysOffset");
        slotActuallyHasItem = tag.getIntArray("SlotActuallyHasItem");

        sideTransferAttempts = tag.getIntArray("SideTransferAttempts");
        sideTransferLatestAttempt = tag.getLongArray("SideTransferLatestAttempt");
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt(RegistryWrapper.WrapperLookup registryLookup) {
        return createNbt(registryLookup);
    }

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        //We provide *this* to the screenHandler as our class Implements Inventory
        //Only the Server has the Inventory at the start, this will be synced to the client in the ScreenHandler
        return new ConveyorBeltScreenHandler(syncId, inv, this);
    }

    public boolean canMoveToSlot(int slot){
        if(slot == 4 || slot == 5){
            return false;//for filter
        }
        return getStack(slot).isEmpty() && (slot == 0 || getStack(slot -1 ).isEmpty()) && (transferCooldownCounter[slot] >= (transferCooldown / 2) || transferCooldown < 4);
    }

    public boolean canMoveHereFromSide(World world, int sideIndex){

        if(sideIndex == 2){
            sideTransferLatestAttempt[2] = world.getTime();//sideTransferLatestAttempt[2] is top side
            return canMoveToSlot(1);
        }

        sideTransferAttempts[sideIndex] += 1;
        sideTransferLatestAttempt[sideIndex] = world.getTime();

        if(sideTransferLatestAttempt[1 - sideIndex] < (world.getTime() - 1)){//if last attempt on other side was more than a second ago, assume no longer trying
            sideTransferAttempts[1 - sideIndex] = 0;
        }

        if(!(sideTransferLatestAttempt[2] < (world.getTime() - 1))){
            return false;//hopper above trying to get in - takes priority
        }

        if(!canMoveToSlot(1)){
            return false;//slot behind target belt trying to get in - takes priority
        }

        return sideTransferAttempts[sideIndex] > sideTransferAttempts[1 - sideIndex];//can go if this side has been trying for longer then the other
    }

    public boolean moveToNextBelt(Direction direction, int fromSlot, ConveyorBeltBlockEntity conveyorBlockEntityInfront){
        if(conveyorBlockEntityInfront == null){
            return false;
        }

        if(!conveyorBlockEntityInfront.getCachedState().get(ConveyorBelt.ENABLED)){
            return false;
        }

        if(conveyorBlockEntityInfront instanceof SlopedConveyorBeltUpBlockEntity){
            if(direction != conveyorBlockEntityInfront.getCachedState().get(ConveyorBelt.FACING)) {
                return false;
            }

            if(!((SlopedConveyorBeltUpBlockEntity)conveyorBlockEntityInfront).copiedItemsFromBelowIfTop && conveyorBlockEntityInfront.getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER){
                return false;//Prevents any items going into the top inventory before it is overwritten by the bottom one
            }

            if(!(conveyorBlockEntityInfront instanceof SlopedConveyorBeltDownBlockEntity) && conveyorBlockEntityInfront.getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.UPPER){
                return false;
            }
        }

        Direction directionOfBeltInfront = conveyorBlockEntityInfront.world.getBlockState(conveyorBlockEntityInfront.pos).get(ConveyorBelt.FACING);

        if(
                conveyorBlockEntityInfront instanceof SlopedConveyorBeltDownBlockEntity &&
                directionOfBeltInfront == direction &&
                conveyorBlockEntityInfront.getCachedState().get(SlopedConveyorBeltUp.HALF) == DoubleBlockHalf.LOWER
        ){
            return false;
        }

        if(directionOfBeltInfront.getOpposite() == direction){
            //belts are facing towards each-over, item can move
            return false;
        }

        if(conveyorBlockEntityInfront instanceof FilterConveyorBeltBlockEntity && directionOfBeltInfront != direction){
            //output of filter belt is facing towards this, can't move
            return false;
        }

        int newOffset = transferSidewaysOffset[fromSlot];
        int newSlot = 0;
        int sideIndex = -1;
        boolean canMove = true;

        if(direction.rotateClockwise(Direction.Axis.Y) == directionOfBeltInfront){
            newOffset = 50;
            newSlot = 1;
            sideIndex = 0;
        }

        if(direction.rotateCounterclockwise(Direction.Axis.Y) == directionOfBeltInfront){
            newOffset = -50;
            newSlot = 1;
            sideIndex = 1;
        }

        if(sideIndex != -1){
            if(!conveyorBlockEntityInfront.canMoveHereFromSide(world, sideIndex)){
                canMove = false;
            }
        }

        if(conveyorBlockEntityInfront.slotActuallyHasItem[newSlot] > 0){
            return false;//Adds a tiny gap between items which prevents some strange behaviour where the cool-downs arnt reset on downward sloping belts when items are in a constant stream.
        }

        if(!canMove){
            return false;
        }

        //move item there
        if(!transferToEmpty(this, fromSlot, conveyorBlockEntityInfront, newSlot)){
            return false;
        }

        if(sideIndex == 0 || sideIndex == 1){
            //going onto left or right side of next belt, cooldown is halved as will be joining halfway into the second slot
            conveyorBlockEntityInfront.transferCooldownCounter[newSlot] = (int)((float)conveyorBlockEntityInfront.transferCooldown * 0.5);
        }
        else{
            //going onto beck of next belt
            conveyorBlockEntityInfront.transferCooldownCounter[newSlot] = conveyorBlockEntityInfront.transferCooldown;
        }

        conveyorBlockEntityInfront.transferSidewaysOffset[newSlot] = newOffset;

        markDirty();
        conveyorBlockEntityInfront.markDirty();

        return true;
    }

    public final boolean moveToNextBelt(Direction direction, int fromSlot){
        ConveyorBeltBlockEntity conveyorBlockEntityInfront = getConveyorBlockEntityAt(world, pos.offset(direction));

        return moveToNextBelt(direction, fromSlot, conveyorBlockEntityInfront);
    }

    public void resetCooldowns(int slot){
        transferCooldownCounter[slot] = transferCooldown;
        transferSidewaysOffset[slot] = 0;
    }

    public void updateCooldowns(int slot){
        transferCooldownCounter[slot] -= 1;

        if (transferCooldownCounter[slot] <= 0) {
            transferCooldownCounter[slot] = 0;
        }

        if(Math.abs(transferSidewaysOffset[slot]) < moveToCenterSpeed){
            transferSidewaysOffset[slot] = 0;
        }
        else if(transferSidewaysOffset[slot] > 0){
            transferSidewaysOffset[slot] -= moveToCenterSpeed;
        }
        else if(transferSidewaysOffset[slot] < 0){
            transferSidewaysOffset[slot] += moveToCenterSpeed;
        }
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, ConveyorBeltBlockEntity blockEntity) {
        if(MinecraftClient.getInstance().isInSingleplayer()) {
            return;
        }

        if (!state.get(ConveyorBelt.ENABLED)) {
            return;
        }

        for (int i = 0; i < 3; i++) {
            blockEntity.updateCooldowns(i);
        }


    }

    public static void serverTick(World world, BlockPos pos, BlockState state, ConveyorBeltBlockEntity blockEntity) {
        if(!state.get(ConveyorBelt.ENABLED)){
            blockEntity.updateSlotActuallyEmptyHack();
            return;
        }

        for(int i = 0; i < 3; i++) {
            if(blockEntity.getStack(i).isEmpty()){
                //if empty slot, reset cool-downs
                blockEntity.resetCooldowns(i);
                continue;
            }

            blockEntity.updateCooldowns(i);

            //if cool-down has run down, meaning item can goto next slot
            if (blockEntity.transferCooldownCounter[i] <= 0) {
                blockEntity.transferCooldownCounter[i] = 0;

                if(i == 2){
                    //if last slot in belt, will goto next belt
                    Direction direction = state.get(ConveyorBelt.FACING);
                    blockEntity.moveToNextBelt(direction, i);
                }
                else {
                    //else, moving from one slot to another within this belt

                    if(transferToEmpty(blockEntity, i, blockEntity, i + 1)){
                        blockEntity.transferCooldownCounter[i + 1] = blockEntity.transferCooldown;
                        blockEntity.transferSidewaysOffset[i + 1] = blockEntity.transferSidewaysOffset[i];
                        blockEntity.markDirty();
                    }
                }
            }
        }

        blockEntity.updateSlotActuallyEmptyHack();
    }

    public static boolean transferToEmpty(Inventory from, int fromSlot, Inventory to, int toSlot) {
        if (!from.getStack(fromSlot).isEmpty() && to.getStack(toSlot).isEmpty()) {

            ItemStack toMove = from.removeStack(fromSlot);
            to.setStack(toSlot, toMove);

            return true;
        }

        return false;
    }

    @Nullable
    public static ConveyorBeltBlockEntity getConveyorBlockEntityAt(World world, BlockPos pos) {
        ConveyorBeltBlockEntity inventory = null;
        BlockState blockState = world.getBlockState(pos);
        if(blockState.hasBlockEntity()){
            Block block = blockState.getBlock();
            if (/*block instanceof InventoryProvider && */block instanceof ConveyorBelt) {
                BlockEntity blockEntity = world.getBlockEntity(pos);
                inventory = (ConveyorBeltBlockEntity)blockEntity;
            }
        }

        return inventory;
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public boolean isEmpty() {
        for(int i = 0; i < items.size(); i++){
            if(!items.get(i).isEmpty()){
                return false;
            }
        }

        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return items.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == items.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = items.get(slot).copy();
        items.get(slot).decrement(amount);
        toRet.setCount(amount);

        //updateSlotActuallyEmptyHack();
        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = items.get(slot).copy();
        items.set(slot, ItemStack.EMPTY);

        //updateSlotActuallyEmptyHack();
        return toRet;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        items.set(slot, stack);

        //updateSlotActuallyEmptyHack();
    }

    public void updateSlotActuallyEmptyHack(){
        int[] slotActuallyHasItemBefore = slotActuallyHasItem.clone();

        Boolean needToUpdate = false;
        for(int i = 0; i < Math.min(slotActuallyHasItem.length, 4); i++){
            slotActuallyHasItem[i] = getStack(i).isEmpty() ? slotActuallyHasItem[i] - 1 : 2;
            if(slotActuallyHasItem[i] < 0){
                slotActuallyHasItem[i] = 0;
            }
            if(
                    slotActuallyHasItem[i] != slotActuallyHasItemBefore[i]// ||
                    //transferCooldownCounter[i] != transferCooldownCounterLastTick[i]
            ){
                needToUpdate = true;
            }
        }

        if(needToUpdate){
            markDirty();
        }

        transferCooldownCounterLastTick = transferCooldownCounter.clone();
    }

    @Override
    public void markDirty() {
        super.markDirty();

        if (this.hasWorld() && !this.getWorld().isClient()) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());//new
        }
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        // Just return an array of all slots
        int[] result = new int[items.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        Direction facing = getCachedState().get(ConveyorBelt.FACING);

        if(!getCachedState().get(ConveyorBelt.ENABLED)){
            return false;
        }

        if(facing.getOpposite() == dir){
            return slot == 0 && canMoveToSlot(0);
        }

        if(slot != 1){
            return false;
        }

        if(dir == Direction.UP){
            return slot == 1 && canMoveHereFromSide(Objects.requireNonNull(getWorld()), 2);
        }

        int sideIndex = -1;

        if(facing.rotateCounterclockwise(Direction.Axis.Y) == dir){
            sideIndex = 0;
        }

        if(facing.rotateClockwise(Direction.Axis.Y) == dir){
            sideIndex = 1;
        }

        if(sideIndex != -1){
            return slot == 1 && canMoveHereFromSide(Objects.requireNonNull(getWorld()), sideIndex);
        }

        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return true;
    }

    public BlockPos getPos(){
        return this.pos;
    }

    @Override
    public void clear() {
        items.clear();
    }
}
