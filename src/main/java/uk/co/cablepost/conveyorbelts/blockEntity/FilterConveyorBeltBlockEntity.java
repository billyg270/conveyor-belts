package uk.co.cablepost.conveyorbelts.blockEntity;

import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.block.ConveyorBelt;
import uk.co.cablepost.conveyorbelts.block.FilterConveyorBelt;
import uk.co.cablepost.conveyorbelts.screenHandler.FilterConveyorBeltScreenHandler;
import uk.co.cablepost.conveyorbelts.util.ItemFilters;

import java.util.Objects;

public class FilterConveyorBeltBlockEntity extends ConveyorBeltBlockEntity {

    public int lastRoundRobinOutDir = 0;

    public int outputMode = 0;
    public static int OUTPUT_MODE_NORMAL = 0;
    public static int OUTPUT_MODE_LEFT_FRONT = 1;
    public static int OUTPUT_MODE_RIGHT_FRONT = 2;

    public FilterConveyorBeltBlockEntity(BlockPos pos, BlockState state, BlockEntityType blockEntityType) {
        super(pos, state, blockEntityType);
        items = DefaultedList.ofSize(6, ItemStack.EMPTY);
        slotActuallyHasItem = new int[size()];

        transferCooldownCounter = new int[4];
        transferSidewaysOffset = new int[4];
    }

    @Override
    public void writeNbt(NbtCompound tag, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.writeNbt(tag, wrapperLookup);

        tag.putInt("LastRoundRobinOutDir", lastRoundRobinOutDir);
        tag.putInt("OutputMode", outputMode);

        //return tag;
    }

    @Override
    public void readNbt(NbtCompound tag, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(tag, wrapperLookup);

        lastRoundRobinOutDir = tag.getInt("LastRoundRobinOutDir");
        outputMode = tag.getInt("OutputMode");
    }

    public static int PROPERTY_DELEGATE_SIZE = 1;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            if (index == 0) {
                return outputMode;
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
            if (index == 0) {
                if(world != null) {
                    outputMode = value;
                    world.setBlockState(pos, getCachedState().with(FilterConveyorBelt.OUTPUT_MODE, outputMode), 3);
                }
            }
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        //We provide *this* to the screenHandler as our class Implements Inventory
        //Only the Server has the Inventory at the start, this will be synced to the client in the ScreenHandler
        return new FilterConveyorBeltScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public boolean canMoveHereFromSide(World world, int sideIndex){
        return false;
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, ConveyorBeltBlockEntity blockEntity) {
        if(!state.get(ConveyorBelt.ENABLED)){
            return;
        }

        for(int i = 0; i < 4; i++) {
            blockEntity.updateCooldowns(i);
        }
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, FilterConveyorBeltBlockEntity blockEntity) {
        if(!state.get(ConveyorBelt.ENABLED)){
            blockEntity.updateSlotActuallyEmptyHack();
            return;
        }

        for(int i = 0; i < 4; i++) {
            if (blockEntity.getStack(i).isEmpty()) {
                //if empty slot, reset cool-downs
                blockEntity.resetCooldowns(i);
                continue;
            }

            blockEntity.updateCooldowns(i);

            if (blockEntity.transferCooldownCounter[i] <= 0) {
                blockEntity.transferCooldownCounter[i] = 0;
                boolean moved = false;

                if (i == 2 || i == 3) {//the last slots for left and right directions
                    Direction direction = state.get(ConveyorBelt.FACING);
                    if (i == 2 && blockEntity.outputMode != OUTPUT_MODE_LEFT_FRONT) {
                        direction = direction.rotateCounterclockwise(Direction.Axis.Y);
                    }
                    if (i == 3 && blockEntity.outputMode != OUTPUT_MODE_RIGHT_FRONT) {
                        direction = direction.rotateClockwise(Direction.Axis.Y);
                    }

                    blockEntity.moveToNextBelt(direction, i);
                } else {
                    int toSlot;

                    if(i == 0){
                        //input slot, another slot after this before filter
                        toSlot = i + 1;
                    }
                    else{
                        //slot to filter
                        toSlot = blockEntity.getToSlotForFilter();
                    }

                    if (toSlot > -1) {
                        if (transferToEmpty(blockEntity, i, blockEntity, toSlot)) {
                            blockEntity.transferCooldownCounter[toSlot] = blockEntity.transferCooldown;
                            blockEntity.transferSidewaysOffset[toSlot] = blockEntity.transferSidewaysOffset[i];

                            if (toSlot > 1) {
                                blockEntity.lastRoundRobinOutDir = toSlot;
                            }

                            blockEntity.markDirty();
                        }
                    } else if (toSlot == -1) {
                        //can't go either way due to filters, is jammed
                        if (world.getTime() % 20 == 0) {
                            world.playSound(pos.getX(), pos.getY(), pos.getZ(), SoundEvents.BLOCK_NOTE_BLOCK_DIDGERIDOO.value(), SoundCategory.BLOCKS, 1.0F, 1.0F, true);
                        }
                    }
                }
            }
        }

        blockEntity.updateSlotActuallyEmptyHack();
    }

    public boolean slotStacksMatch(int inputSlot, int filterSlot){
        return ItemFilters.Filter(getStack(filterSlot), getStack(inputSlot));
    }

    public int getToSlotForFilter(){
        if(
                getStack(4).isEmpty() && getStack(5).isEmpty() ||
                (
                        slotStacksMatch(1, 4) &&
                        slotStacksMatch(1, 5)
                )
        ) {
            //both filters are empty or both match input, could go either way

            if (getStack(2).isEmpty() && getStack(3).isEmpty()) {
                //both target slots are empty

                //pick whichever one was not used last
                if (lastRoundRobinOutDir != 2) {
                    return 2;
                }
                return 3;
            }

            //if only one target is empty, go there
            if (getStack(2).isEmpty()) {
                return 2;
            }

            if (getStack(3).isEmpty()) {
                return 3;
            }

            //no target slots are empty
            return -2;//not clogged, just backed up
        }

        if(slotStacksMatch(1, 4)){
            return 2;
        }

        if(slotStacksMatch(1, 5)){
            return 3;
        }

        if(getStack(4).isEmpty()){
            return 2;
        }

        if(getStack(5).isEmpty()){
            return 3;
        }

        return -1;
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(!getCachedState().get(ConveyorBelt.ENABLED)){
            return false;
        }

        Direction facing = getCachedState().get(ConveyorBelt.FACING);

        if(facing.getOpposite() == dir){
            return slot == 0 && canMoveToSlot(0);
        }

        if(dir == Direction.UP){
            return slot == 1 && canMoveHereFromSide(Objects.requireNonNull(getWorld()), 2);
        }

        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        if(slot == 4 || slot == 5){
            return false;
        }
        return true;
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        return new int[] { 0, 1, 2, 3 };
    }
}
