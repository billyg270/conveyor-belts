package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public class IronConveyorBeltBlockEntity extends ConveyorBeltBlockEntity {
    public IronConveyorBeltBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.IRON_CONVEYOR_BELT_BLOCK_ENTITY);
        this.transferCooldown = ConveyorBelts.IRON_TRANSFER_COOLDOWN;
        this.moveToCenterSpeed = ConveyorBelts.IRON_MOVE_TO_CENTER_SPEED;
    }
}
