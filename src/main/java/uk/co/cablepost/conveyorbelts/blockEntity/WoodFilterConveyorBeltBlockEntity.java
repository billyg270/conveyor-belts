package uk.co.cablepost.conveyorbelts.blockEntity;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public class WoodFilterConveyorBeltBlockEntity extends FilterConveyorBeltBlockEntity{

    public WoodFilterConveyorBeltBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY);
        this.transferCooldown = ConveyorBelts.WOOD_TRANSFER_COOLDOWN;
        this.moveToCenterSpeed = ConveyorBelts.WOOD_MOVE_TO_CENTER_SPEED;
    }
}
