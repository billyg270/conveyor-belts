package uk.co.cablepost.conveyorbelts.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.client.util.InputUtil;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;
import org.lwjgl.glfw.GLFW;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntityRenderer.ConveyorBeltBlockEntityRenderer;
import uk.co.cablepost.conveyorbelts.packets.CycleBeltElevationPacket;
import uk.co.cablepost.conveyorbelts.packets.UpdateInvertBeltPlacementPacket;
import uk.co.cablepost.conveyorbelts.packets.UpdateRoboticArmInsertModePacket;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntityRenderer;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmModel;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmScreen;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmScreen;
import uk.co.cablepost.conveyorbelts.screen.ConveyorBeltScreen;
import uk.co.cablepost.conveyorbelts.screen.FilterConveyorBeltScreen;

@Environment(EnvType.CLIENT)
public class ConveyorBeltsClient implements ClientModInitializer {

    public static KeyBinding beltPlacementKeyBinding;
    public static boolean invertBeltPlacement = false;


    public static final EntityModelLayer ROBOTIC_ARM_MODEL_LAYER = new EntityModelLayer(Identifier.of(ConveyorBelts.MOD_ID, "robotic_arm_model_layer"), "main");//https://discord.com/channels/507304429255393322/807617488313516032/971248602196377600

    @Override
    public void onInitializeClient() {

        //BLOCK ENTITY RENDERERS

        //==== REGULAR ====

        //wood
        BlockEntityRendererRegistry.register(ConveyorBelts.WOOD_CONVEYOR_BELT_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //iron
        BlockEntityRendererRegistry.register(ConveyorBelts.IRON_CONVEYOR_BELT_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //gold
        BlockEntityRendererRegistry.register(ConveyorBelts.GOLD_CONVEYOR_BELT_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //==== SLOPED UP ====

        //wood
        BlockEntityRendererRegistry.register(ConveyorBelts.WOOD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //iron
        BlockEntityRendererRegistry.register(ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //gold
        BlockEntityRendererRegistry.register(ConveyorBelts.GOLD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //==== SLOPED DOWN ====

        //wood
        BlockEntityRendererRegistry.register(ConveyorBelts.WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //iron
        BlockEntityRendererRegistry.register(ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //gold
        BlockEntityRendererRegistry.register(ConveyorBelts.GOLD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //==== FILTER ====

        //wood
        BlockEntityRendererRegistry.register(ConveyorBelts.WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //iron
        BlockEntityRendererRegistry.register(ConveyorBelts.IRON_FILTER_CONVEYOR_BELT_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        //gold
        BlockEntityRendererRegistry.register(ConveyorBelts.GOLD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY, ConveyorBeltBlockEntityRenderer::new);

        // Conveyor SCREENS
        HandledScreens.register(ConveyorBelts.CONVEYOR_BELT_SCREEN_HANDLER, ConveyorBeltScreen::new);
        HandledScreens.register(ConveyorBelts.FILTER_CONVEYOR_BELT_SCREEN_HANDLER, FilterConveyorBeltScreen::new);

        //Robotic arm
        EntityModelLayerRegistry.registerModelLayer(ROBOTIC_ARM_MODEL_LAYER, RoboticArmModel::getTexturedModelData);

        BlockEntityRendererRegistry.register(ConveyorBelts.WOOD_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(ConveyorBelts.IRON_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(ConveyorBelts.GOLD_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(ConveyorBelts.NETHERITE_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);

        BlockEntityRendererRegistry.register(ConveyorBelts.WOOD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(ConveyorBelts.IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(ConveyorBelts.GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(ConveyorBelts.NETHERITE_FILTER_ROBOTIC_ARM_BLOCK_ENTITY, RoboticArmBlockEntityRenderer::new);

        // Arm SCREENS
        HandledScreens.register(ConveyorBelts.ROBOTIC_ARM_SCREEN_HANDLER, RoboticArmScreen::new);
        HandledScreens.register(ConveyorBelts.FILTER_ROBOTIC_ARM_SCREEN_HANDLER, FilterRoboticArmScreen::new);


        //KEY BINDS

        beltPlacementKeyBinding = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                "key.conveyorbelts.belt_key", // The translation key of the keybinding's name
                InputUtil.Type.KEYSYM, // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
                GLFW.GLFW_KEY_R, // The keycode of the key
                "category.conveyorbelts" // The translation key of the keybinding's category.
        ));

        //EVENTS

        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            while (beltPlacementKeyBinding.wasPressed()) {
                onBeltKey();
            }
        });

        ClientPlayConnectionEvents.JOIN.register((handler, sender, client) -> {
            invertBeltPlacement = true;
            invertBeltPlacement(false);
        });
    }

    public static void onBeltKey(){
        MinecraftClient client = MinecraftClient.getInstance();
        if(client.player == null){
            return;
        }

        if(client.player.isSneaking()){
            cycleBeltElevationType();
        }
        else {
            invertBeltPlacement(true);
        }
    }

    public static void invertBeltPlacement(boolean sendChatMessage){
        invertBeltPlacement = !invertBeltPlacement;
        ClientPlayNetworking.send(new UpdateInvertBeltPlacementPacket(invertBeltPlacement, sendChatMessage));
    }

    public static void cycleBeltElevationType(){
        ClientPlayNetworking.send(new CycleBeltElevationPacket(false));
    }
}
