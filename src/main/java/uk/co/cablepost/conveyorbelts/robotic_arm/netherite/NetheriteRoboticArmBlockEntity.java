package uk.co.cablepost.conveyorbelts.robotic_arm.netherite;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.HopperBlockEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.ConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.util.GetCanMoveResp;

import java.util.stream.IntStream;

public class NetheriteRoboticArmBlockEntity extends RoboticArmBlockEntity {
    public NetheriteRoboticArmBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.NETHERITE_ROBOTIC_ARM_BLOCK_ENTITY);
        this.maxMovementProgress = ConveyorBelts.NETHERITE_ROBOTIC_ARM_SPEED;
        this.maxStackSize = 10000;
    }
}
