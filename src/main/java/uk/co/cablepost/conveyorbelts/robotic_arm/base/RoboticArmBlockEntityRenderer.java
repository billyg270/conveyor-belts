package uk.co.cablepost.conveyorbelts.robotic_arm.base;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.render.model.json.ModelTransformationMode;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.RotationAxis;
import org.joml.Vector3f;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.client.ConveyorBeltsClient;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodRoboticArmBlockEntity;

import java.util.Objects;

@Environment(value = EnvType.CLIENT)
public class RoboticArmBlockEntityRenderer<T extends RoboticArmBlockEntity> implements BlockEntityRenderer<T> {

    public SpriteIdentifier ARM_MODEL_TEXTURE = null;
    public RoboticArmModel armModel;

    public RoboticArmBlockEntityRenderer(BlockEntityRendererFactory.Context ctx) {
        this.armModel = new RoboticArmModel(ctx.getLayerModelPart(ConveyorBeltsClient.ROBOTIC_ARM_MODEL_LAYER));
    }

    @Override
    public void render(RoboticArmBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {

        if(ARM_MODEL_TEXTURE == null){
            if(blockEntity instanceof WoodRoboticArmBlockEntity || blockEntity instanceof WoodFilterRoboticArmBlockEntity){
                ARM_MODEL_TEXTURE = new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, Identifier.of(ConveyorBelts.MOD_ID, "block/wood_robotic_arm_model"));
            }
            if(blockEntity instanceof IronRoboticArmBlockEntity || blockEntity instanceof IronFilterRoboticArmBlockEntity){
                ARM_MODEL_TEXTURE = new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, Identifier.of(ConveyorBelts.MOD_ID, "block/iron_robotic_arm_model"));
            }
            if(blockEntity instanceof GoldRoboticArmBlockEntity || blockEntity instanceof GoldFilterRoboticArmBlockEntity){
                ARM_MODEL_TEXTURE = new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, Identifier.of(ConveyorBelts.MOD_ID, "block/gold_robotic_arm_model"));
            }
            if(blockEntity instanceof NetheriteRoboticArmBlockEntity || blockEntity instanceof NetheriteFilterRoboticArmBlockEntity){
                ARM_MODEL_TEXTURE = new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, Identifier.of(ConveyorBelts.MOD_ID, "block/netherite_robotic_arm_model"));
            }
        }

        matrices.push();

        matrices.translate(0.5, 1.5, 0.5);
        matrices.multiply(RotationAxis.of(new Vector3f(1, 0, 0)).rotationDegrees(180));

        Direction facing = blockEntity.getCachedState().get(RoboticArmBlock.FACING);

        float moveProgress = (float)blockEntity.movementProgress / (float)blockEntity.maxMovementProgress;

        if(blockEntity.actuallyHoldingItem){
            moveProgress = 1f - moveProgress;
        }

        float baseRotation = 0f;

        if (facing == Direction.NORTH) {
            baseRotation = 180f;
        } else if (facing == Direction.EAST) {
            baseRotation = 270f;
        } else if (facing == Direction.SOUTH) {
            baseRotation = 0f;
        } else if (facing == Direction.WEST) {
            baseRotation = 90f;
        }

        //matrices.scale(0.439f, 0.439f, 0.439f);

        float shoulderYaw = baseRotation + (moveProgress * 180f);

        float reachMul = 40f;

        Direction reachingToDirection = facing;

        if(moveProgress > 0.5f) {//input side
            reachingToDirection = facing.getOpposite();
        }

        if(blockEntity.getWorld() != null) {
            BlockState reachingToBlockState = blockEntity.getWorld().getBlockState(blockEntity.getPos().offset(reachingToDirection));

            double maxYofOutline;

            if(reachingToBlockState.getBlock() instanceof RoboticArmBlock){
                maxYofOutline = 1f;
            }
            else {
                maxYofOutline = reachingToBlockState.getBlock().getOutlineShape(
                        reachingToBlockState,
                        blockEntity.getWorld(),
                        blockEntity.getPos(),
                        ShapeContext.absent()
                ).getMax(Direction.Axis.Y);
            }

            if(maxYofOutline > 0 && maxYofOutline < 2) {
                reachMul += (1f - maxYofOutline) * 120f;
            }
        }

        float arm1pitch = -45f + ((float)Math.pow(moveProgress - 0.5f, 2f) * 2f * reachMul);
        float elbowPitch = 120f - ((float)Math.pow(moveProgress - 0.5f, 2f) * 1f * reachMul);

        float degToRad = 0.0174533f;
        armModel.setArmAngles(
                shoulderYaw * degToRad,
                arm1pitch * degToRad,
                elbowPitch * degToRad
        );

        VertexConsumer vertexConsumer = ARM_MODEL_TEXTURE.getVertexConsumer(vertexConsumers, RenderLayer::getEntitySolid);
        armModel.render(matrices, vertexConsumer, light, overlay, 0xFFFFFFFF);

        if(blockEntity.actuallyHoldingItem && !blockEntity.getStack(0).isEmpty()){
            ItemStack itemStack = blockEntity.getStack(0);

            boolean hasDepth = Objects.requireNonNull(MinecraftClient.getInstance().getItemRenderer().getModels().getModel(itemStack.getItem())).hasDepth();

            matrices.translate(0, 1.5f, 0);
            //matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(shoulderYaw));
            matrices.multiply(RotationAxis.of(new Vector3f(0, 1, 0)).rotationDegrees(shoulderYaw));
            //matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(arm1pitch));
            matrices.multiply(RotationAxis.of(new Vector3f(1, 0, 0)).rotationDegrees(arm1pitch));
            matrices.translate(0, -0.7, 0);
            //matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(elbowPitch));
            matrices.multiply(RotationAxis.of(new Vector3f(1, 0, 0)).rotationDegrees(elbowPitch));
            matrices.translate(0, -0.8, 0);
            //matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(90));
            matrices.multiply(RotationAxis.of(new Vector3f(1, 0, 0)).rotationDegrees(90));

            if(hasDepth){
                matrices.translate(0, -0.07f, 0.05f);
                matrices.scale(1.1f, 1.1f, 1.1f);
            }
            else{
                matrices.translate(0, 0.02, 0.02f);
                matrices.scale(0.65f, 0.65f, 0.65f);
            }


            MinecraftClient.getInstance().getItemRenderer().renderItem(itemStack, ModelTransformationMode.GROUND, light, overlay, matrices, vertexConsumers, blockEntity.getWorld(), 0);
        }

        matrices.pop();
    }
}
