package uk.co.cablepost.conveyorbelts.robotic_arm.filter;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmScreen;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmScreenHandler;

public class FilterRoboticArmScreen extends RoboticArmScreen {
    public FilterRoboticArmScreen(RoboticArmScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        TEXTURE = Identifier.of(ConveyorBelts.MOD_ID, "textures/gui/container/filter_robotic_arm.png");
    }
}
