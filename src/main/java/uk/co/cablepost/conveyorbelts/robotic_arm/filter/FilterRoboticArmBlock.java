package uk.co.cablepost.conveyorbelts.robotic_arm.filter;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlock;

public abstract class FilterRoboticArmBlock extends RoboticArmBlock {
    public FilterRoboticArmBlock(Settings settings, boolean skipSetDefaultState) {
        super(settings, skipSetDefaultState);
    }

    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        FilterRoboticArmBlockEntity blockEntity = (FilterRoboticArmBlockEntity) world.getBlockEntity(pos);

        if(blockEntity == null){
            return 0;
        }

        int i = 0;
        float f = 0.0F;

        ItemStack itemStack = blockEntity.getStack(0);
        if (!itemStack.isEmpty()) {
            f += (float)itemStack.getCount() / (float)Math.min(blockEntity.getMaxCountPerStack(), itemStack.getMaxCount());
            ++i;
        }

        f /= (float)4;
        return MathHelper.floor(f * 14.0F) + (i > 0 ? 1 : 0);
    }
}
