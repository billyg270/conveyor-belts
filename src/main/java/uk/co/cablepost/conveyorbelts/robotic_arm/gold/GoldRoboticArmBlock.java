package uk.co.cablepost.conveyorbelts.robotic_arm.gold;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodRoboticArmBlock;

public class GoldRoboticArmBlock extends RoboticArmBlock {

    public GoldRoboticArmBlock(Settings settings) {
        super(settings, false);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new GoldRoboticArmBlockEntity(pos, state);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return createCodec(GoldRoboticArmBlock::new);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? validateTicker(type, ConveyorBelts.GOLD_ROBOTIC_ARM_BLOCK_ENTITY, GoldRoboticArmBlockEntity::clientTick) : validateTicker(type, ConveyorBelts.GOLD_ROBOTIC_ARM_BLOCK_ENTITY, GoldRoboticArmBlockEntity::serverTick);
    }
}
