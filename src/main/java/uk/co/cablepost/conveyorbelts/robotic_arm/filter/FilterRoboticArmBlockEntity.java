package uk.co.cablepost.conveyorbelts.robotic_arm.filter;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.util.GetCanMoveResp;
import uk.co.cablepost.conveyorbelts.util.ItemFilters;

public class FilterRoboticArmBlockEntity extends RoboticArmBlockEntity {
    public FilterRoboticArmBlockEntity(BlockPos pos, BlockState state, BlockEntityType blockEntityType) {
        super(pos, state, blockEntityType);
        items = DefaultedList.ofSize(2, ItemStack.EMPTY);
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new FilterRoboticArmScreenHandler(syncId, inv, this, propertyDelegate);
    }

    @Override
    public GetCanMoveResp getCanMove(ItemStack itemStack, Inventory outputInventory, int[] slots, Direction direction){
        ItemStack filterStack = getStack(1);
        if(!filterStack.isEmpty() && !ItemFilters.Filter(filterStack, itemStack)){
            return new GetCanMoveResp();
        }

        return super.getCanMove(itemStack, outputInventory, slots, direction);
    }
}
