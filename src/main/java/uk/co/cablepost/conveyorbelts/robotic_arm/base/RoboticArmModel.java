package uk.co.cablepost.conveyorbelts.robotic_arm.base;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.*;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.util.math.MatrixStack;

@Environment(value = EnvType.CLIENT)

public class RoboticArmModel extends Model {
    private static final String SHOULDER = "shoulder";
    private static final String ARM_1 = "arm_1";
    private static final String ELBOW = "elbow";
    private static final String ARM_2 = "arm_2";
    private static final String HAND = "hand";
    private final ModelPart root;

    private final ModelPart shoulder;
    private final ModelPart arm_1;
    private final ModelPart elbow;
    private final ModelPart arm_2;
    private final ModelPart hand;

    public RoboticArmModel(ModelPart root) {
        super(RenderLayer::getEntitySolid);
        this.root = root;

        this.shoulder = root.getChild(SHOULDER);
        this.arm_1 = shoulder.getChild(ARM_1);
        this.elbow = arm_1.getChild(ELBOW);
        this.arm_2 = elbow.getChild(ARM_2);
        this.hand = arm_2.getChild(HAND);
    }

    public static TexturedModelData getTexturedModelData() {
        ModelData modelData = new ModelData();
        ModelPartData modelPartData = modelData.getRoot();

        ModelPartData shoulder = modelPartData.addChild(
                SHOULDER,
                ModelPartBuilder
                        .create()
                        .uv(0, 0)
                        .cuboid(-2.0F, -2.0F, -2.0F, 4.0F, 4.0F, 4.0F),
                ModelTransform.pivot(0.0F, 21.0F, 0.0F)
        );

        ModelPartData arm_1 = shoulder.addChild(
                ARM_1,
                ModelPartBuilder
                        .create()
                        .uv(19, 0)
                        .cuboid(-1.0F, -10.0F, -1.0F, 2.0F, 8.0F, 2.0F),
                ModelTransform.pivot(0.0F, 1.0F, 0.0F)
        );

        ModelPartData elbow = arm_1.addChild(
                ELBOW,
                ModelPartBuilder
                        .create()
                        .uv(0, 0)
                        .cuboid(-2.0F, -2.0F, -2.0F, 4.0F, 4.0F, 4.0F),
                ModelTransform.pivot(0.0F, -11.0F, 0.0F)
        );

        ModelPartData arm_2 = elbow.addChild(
                ARM_2,
                ModelPartBuilder
                        .create()
                        .uv(19, 0)
                        .cuboid(-1.0F, -9.0F, -1.0F, 2.0F, 8.0F, 2.0F),
                ModelTransform.pivot(0.0F, 0.0F, 0.0F)
        );

        /*
        arm_2.addChild(
                HAND,
                ModelPartBuilder
                        .create()
                        .uv(0, 14)
                        .cuboid(-4.0F, -2.0F, -2.0F, 8.0F, 2.0F, 4.0F),
                ModelTransform.pivot(0.0F, -8.0F, 0.0F)
        );

        arm_2.addChild(
                "finger_1",
                ModelPartBuilder
                        .create()
                        .uv(0, 25)
                        .cuboid(3.0F, -5.0F, -2.0F, 1.0F, 3.0F, 4.0F),
                ModelTransform.pivot(0.0F, -8.0F, 0.0F)
        );

        arm_2.addChild(
                "finger_2",
                ModelPartBuilder
                        .create()
                        .uv(0, 25)
                        .cuboid(-4.0F, -5.0F, -2.0F, 1.0F, 3.0F, 4.0F),
                ModelTransform.pivot(0.0F, -8.0F, 0.0F)
        );
        */

        arm_2.addChild(
                HAND,
                ModelPartBuilder
                        .create()
                        .uv(0, 14)
                        .cuboid(-3.0F, -2.0F, -2.0F, 6.0F, 2.0F, 4.0F),
                ModelTransform.pivot(0.0F, -8.0F, 0.0F)
        );

        arm_2.addChild(
                "finger_1",
                ModelPartBuilder
                        .create()
                        .uv(0, 25)
                        .cuboid(2.0F, -5.0F, -2.0F, 1.0F, 3.0F, 4.0F),
                ModelTransform.pivot(0.0F, -8.0F, 0.0F)
        );

        arm_2.addChild(
                "finger_2",
                ModelPartBuilder
                        .create()
                        .uv(0, 25)
                        .cuboid(-3.0F, -5.0F, -2.0F, 1.0F, 3.0F, 4.0F),
                ModelTransform.pivot(0.0F, -8.0F, 0.0F)
        );

        return TexturedModelData.of(modelData, 64, 32);
    }

    @Override
    public void render(MatrixStack matrices, VertexConsumer vertices, int light, int overlay, int color) {
        this.renderArm(matrices, vertices, light, overlay, color);
    }

    public void renderArm(MatrixStack matrices, VertexConsumer vertices, int light, int overlay, int color) {
        this.root.render(matrices, vertices, light, overlay, color);
    }

    public void setArmAngles(float shoulderYaw, float arm_1_pitch, float elbow_pitch) {
        shoulder.yaw = shoulderYaw;
        arm_1.pitch = arm_1_pitch;
        elbow.pitch = elbow_pitch;
    }
}
