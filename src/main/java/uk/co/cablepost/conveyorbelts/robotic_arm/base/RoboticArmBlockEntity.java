package uk.co.cablepost.conveyorbelts.robotic_arm.base;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.HopperBlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.registry.RegistryWrapper;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.blockEntity.ConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.util.GetCanMoveResp;

import java.util.stream.IntStream;

public class RoboticArmBlockEntity extends BlockEntity implements NamedScreenHandlerFactory, SidedInventory, Inventory {
    public DefaultedList<ItemStack> items = DefaultedList.ofSize(1, ItemStack.EMPTY);

    public int movementProgress = 0;
    public int maxMovementProgress = 1000;
    public int maxStackSize = 1;
    public boolean actuallyHoldingItem = false;

    public int insertMode = 0;

    public static int INSERT_MODE_NONE = -1;
    public static int INSERT_MODE_SIDE = 0;
    public static int INSERT_MODE_TOP = 1;
    public static int INSERT_MODE_SIDE_TOP = 2;

    public RoboticArmBlockEntity(BlockPos pos, BlockState state, BlockEntityType blockEntityType) {
        super(blockEntityType, pos, state);
    }

    @Override
    public void writeNbt(NbtCompound tag, RegistryWrapper.WrapperLookup wrapperLookup) {

        Inventories.writeNbt(tag, items, wrapperLookup);
        tag.putInt("MovementProgress", movementProgress);
        //tag.putInt("MaxMovementProgress", maxMovementProgress);
        tag.putBoolean("ActuallyHoldingItem", actuallyHoldingItem);

        tag.putInt("InsertMode", insertMode);

        super.writeNbt(tag, wrapperLookup);
    }

    @Override
    public void readNbt(NbtCompound tag, RegistryWrapper.WrapperLookup wrapperLookup) {
        super.readNbt(tag, wrapperLookup);

        Inventories.readNbt(tag, items, wrapperLookup);
        movementProgress = tag.getInt("MovementProgress");
        //maxMovementProgress = tag.getInt("MaxMovementProgress");
        //if(maxMovementProgress == 0){
        //    maxMovementProgress = 100;
        //}
        actuallyHoldingItem = tag.getBoolean("ActuallyHoldingItem");

        insertMode = tag.getInt("InsertMode");
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt(RegistryWrapper.WrapperLookup registryLookup) {
        return createNbt(registryLookup);
    }

    public static int PROPERTY_DELEGATE_SIZE = 1;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            if (index == 0) {
                return insertMode;
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
            if (index == 0) {
                insertMode = value;
            }
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    @Override
    public Text getDisplayName() {
        return Text.translatable(getCachedState().getBlock().getTranslationKey());
    }

    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        //We provide *this* to the screenHandler as our class Implements Inventory
        //Only the Server has the Inventory at the start, this will be synced to the client in the ScreenHandler
        return new RoboticArmScreenHandler(syncId, inv, this, propertyDelegate);
    }

    public static void clientTick(World world, BlockPos pos, BlockState state, RoboticArmBlockEntity blockEntity) {

        if(!state.get(RoboticArmBlock.ENABLED)){
            return;
        }

        if(blockEntity.movementProgress >= 1 && blockEntity.movementProgress < blockEntity.maxMovementProgress){
            blockEntity.movementProgress ++;
        }
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, RoboticArmBlockEntity blockEntity) {
        if(!state.get(RoboticArmBlock.ENABLED)){
            return;
        }

        boolean toMarkDirty = false;

        if(blockEntity.movementProgress < blockEntity.maxMovementProgress){
            blockEntity.movementProgress ++;
            if(blockEntity.movementProgress == 1){
                toMarkDirty = true;//So client knows has started moving
            }
        }

        if(blockEntity.movementProgress >= blockEntity.maxMovementProgress) {
            ItemStack itemStack = blockEntity.getStack(0);

            Inventory outputInventory = HopperBlockEntity.getInventoryAt(world, pos.offset(state.get(RoboticArmBlock.FACING)));

            int[] outputAvailableSlots = new int[0];
            int[] outputAvailableSlotsFallback = new int[0];

            Direction insertDirection = state.get(RoboticArmBlock.FACING).getOpposite();
            Direction fallbackInsertDirection = Direction.UP;

            if(outputInventory != null) {
                if (outputInventory instanceof SidedInventory si) {
                    if(blockEntity.insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE || blockEntity.insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE_TOP) {
                        outputAvailableSlots = si.getAvailableSlots(state.get(RoboticArmBlock.FACING).getOpposite());
                    }
                    if(blockEntity.insertMode == RoboticArmBlockEntity.INSERT_MODE_TOP || blockEntity.insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE_TOP) {
                        outputAvailableSlotsFallback = si.getAvailableSlots(Direction.UP);
                    }

                    if(outputInventory instanceof ConveyorBeltBlockEntity && !(outputInventory instanceof FilterConveyorBeltBlockEntity)){
                        outputAvailableSlots = new int[0];
                        outputAvailableSlotsFallback = new int[]{1};
                    }
                }
                else{
                    if(blockEntity.insertMode != RoboticArmBlockEntity.INSERT_MODE_NONE) {
                        outputAvailableSlots = IntStream.range(0, outputInventory.size()).toArray();
                    }
                }
            }

            if (itemStack.isEmpty()) {
                //If can insert into target
                //Try to pick that type of item up

                Inventory inputInventory = HopperBlockEntity.getInventoryAt(world, pos.offset(state.get(RoboticArmBlock.FACING).getOpposite()));

                if (inputInventory != null) {
                    int[] inputAvailableSlots = IntStream.range(0, inputInventory.size()).toArray();
                    if (inputInventory instanceof SidedInventory si) {
                        inputAvailableSlots = si.getAvailableSlots(Direction.DOWN);
                    }

                    for (int slot : inputAvailableSlots) {
                        ItemStack itemStackToExtract = inputInventory.getStack(slot);
                        if (itemStackToExtract.isEmpty()) {
                            continue;
                        }
                        if (inputInventory instanceof SidedInventory sidedInputInventory) {
                            //if (!sidedInputInventory.canExtract(slot, itemStackToExtract, state.get(RoboticArmBlock.FACING))) {
                            if (!sidedInputInventory.canExtract(slot, itemStackToExtract, Direction.DOWN)) {
                                continue;
                            }
                        }

                        //GetCanMoveResp canMove = blockEntity.getCanMove(itemStackToExtract, outputInventory, ArrayUtils.addAll(outputAvailableSlots, outputAvailableSlotsFallback), insertDirection);
                        GetCanMoveResp canMove = blockEntity.getCanMoveMultiple(itemStackToExtract, outputInventory, outputAvailableSlots, outputAvailableSlotsFallback, insertDirection, fallbackInsertDirection);

                        if (canMove.canMove > 0 && outputInventory != null) {
                            canMove.canMove = Math.min(canMove.canMove, blockEntity.maxStackSize);//Take 1 item at a time

                            ItemStack itemStackTaken = itemStackToExtract.copy();
                            itemStackTaken.setCount(canMove.canMove);

                            blockEntity.setStack(0, itemStackTaken);
                            inputInventory.removeStack(slot, canMove.canMove);
                            inputInventory.markDirty();
                            toMarkDirty = true;
                            break;
                        }
                    }
                }
            } else {
                //Try to insert into target
                //At side then at top
                //GetCanMoveResp canMove = blockEntity.getCanMove(itemStack, outputInventory, ArrayUtils.addAll(outputAvailableSlots, outputAvailableSlotsFallback), insertDirection);
                GetCanMoveResp canMove = blockEntity.getCanMoveMultiple(itemStack, outputInventory, outputAvailableSlots, outputAvailableSlotsFallback, insertDirection, fallbackInsertDirection);

                if (canMove.canMove > 0 && outputInventory != null) {
                    canMove.canMove = Math.min(canMove.canMove, blockEntity.maxStackSize);//Place 1 item at a time


                    ItemStack insertedStack = canMove.itemStackToInsertOn.isEmpty() ? itemStack.copy() : canMove.itemStackToInsertOn.copy();
                    if (!canMove.itemStackToInsertOn.isEmpty()) {
                        insertedStack.setCount(insertedStack.getCount() + canMove.canMove);
                    }
                    else{
                        insertedStack.setCount(canMove.canMove);
                    }
                    outputInventory.setStack(canMove.slot, insertedStack);
                    blockEntity.removeStack(0, canMove.canMove);
                    outputInventory.markDirty();
                    toMarkDirty = true;
                }
            }
        }

        if(toMarkDirty){
            blockEntity.markDirty();
        }
    }

    public GetCanMoveResp getCanMoveMultiple(
            ItemStack itemStack,
            Inventory outputInventory,
            int[] outputAvailableSlots,
            int[] outputAvailableSlotsFallback,
            Direction insertDirection,
            Direction fallbackInsertDirection
    ){
        GetCanMoveResp canMove = getCanMove(itemStack, outputInventory, outputAvailableSlots, insertDirection);

        if(canMove.canMove > 0){
            return canMove;
        }

        return getCanMove(itemStack, outputInventory, outputAvailableSlotsFallback, fallbackInsertDirection);
    }

    public GetCanMoveResp getCanMove(ItemStack itemStack, Inventory outputInventory, int[] slots, Direction direction){

        if(itemStack.isEmpty()){
            return new GetCanMoveResp();
        }

        if (outputInventory == null) {
            return new GetCanMoveResp();
        }

        for (int slot : slots) {
            ItemStack itemStackToInsertOn = outputInventory.getStack(slot);
            if (outputInventory instanceof SidedInventory sidedInputInventory) {
                if (!sidedInputInventory.canInsert(slot, itemStack, direction)) {
                    continue;
                }
            }
            if (!itemStackToInsertOn.isEmpty() && !ItemStack.areItemsAndComponentsEqual(itemStack, itemStackToInsertOn)) {
                continue;
            }
            int canMove = itemStackToInsertOn.isEmpty() ? itemStack.getMaxCount() : itemStackToInsertOn.getMaxCount() - itemStackToInsertOn.getCount();

            if (canMove == 0) {
                continue;
            }

            canMove = Math.min(canMove, itemStack.getCount());

            return new GetCanMoveResp(canMove, itemStackToInsertOn, slot);
        }

        return new GetCanMoveResp();
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack item : items) {
            if (!item.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return items.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == items.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = items.get(slot).copy();
        items.get(slot).decrement(amount);
        toRet.setCount(amount);

        movementProgress = maxMovementProgress - 1;

        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = items.get(slot).copy();
        items.set(slot, ItemStack.EMPTY);

        movementProgress = 0;

        return toRet;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        items.set(slot, stack);

        movementProgress = 0;
    }

    @Override
    public void markDirty() {

        boolean isServer = this.hasWorld() && !this.getWorld().isClient();

        if(isServer){
            actuallyHoldingItem = !getStack(0).isEmpty();
        }

        super.markDirty();

        if(isServer) {
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        return new int[]{0};
    }

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        return slot == 0 && movementProgress >= maxMovementProgress;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot == 0 && movementProgress >= maxMovementProgress;
    }

    @Override
    public void clear() {
        items.clear();
    }
}
