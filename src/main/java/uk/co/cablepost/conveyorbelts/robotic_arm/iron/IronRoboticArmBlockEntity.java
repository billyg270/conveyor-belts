package uk.co.cablepost.conveyorbelts.robotic_arm.iron;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmBlockEntity;

public class IronRoboticArmBlockEntity extends RoboticArmBlockEntity {
    public IronRoboticArmBlockEntity(BlockPos pos, BlockState state) {
        super(pos, state, ConveyorBelts.IRON_ROBOTIC_ARM_BLOCK_ENTITY);
        this.maxMovementProgress = ConveyorBelts.IRON_ROBOTIC_ARM_SPEED;
    }
}
