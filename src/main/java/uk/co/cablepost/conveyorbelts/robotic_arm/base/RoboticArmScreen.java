package uk.co.cablepost.conveyorbelts.robotic_arm.base;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.packets.UpdateRoboticArmInsertModePacket;

public class RoboticArmScreen extends HandledScreen<RoboticArmScreenHandler> {
    public Identifier TEXTURE = Identifier.of(ConveyorBelts.MOD_ID, "textures/gui/container/robotic_arm.png");

    public boolean insertSide;
    public boolean insertTop;

    public RoboticArmScreen(RoboticArmScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        //Insert insertMode select

        context.drawText(this.textRenderer, Text.of("Insert:"), x + 120, y + 27, 4210752, false);
        context.drawText(this.textRenderer, Text.of("Side"), x + 131, y + 39, 4210752, false);
        context.drawText(this.textRenderer, Text.of("Top"), x + 131, y + 51, 4210752, false);

        getInsertMode();//Do it each frame in-case another player is updating it also

        if(insertSide){
            context.drawText(this.textRenderer, Text.of("x"), x + 121, y + 39, 0xffffff, false);
        }

        if(insertTop){
            context.drawText(this.textRenderer, Text.of("x"), x + 121, y + 51, 0xffffff, false);
        }
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (client == null || client.player == null || client.player.isSpectator()) {
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        double mx = mouseX - x;
        double my = mouseY - y;

        if(mx >= 120 && mx <= 127){
            if(my >= 40 && my <= 47){
                insertSide = !insertSide;
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                applyInsertMode();
            }

            if(my >= 52 && my <= 59){
                insertTop = !insertTop;
                client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                applyInsertMode();
            }
        }

        return super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        super.render(context, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(context, mouseX, mouseY);
    }


    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }

    private void applyInsertMode(){
        int toSend = 0;
        if(insertSide && insertTop){
            toSend = RoboticArmBlockEntity.INSERT_MODE_SIDE_TOP;
        }
        else if(insertSide){
            toSend = RoboticArmBlockEntity.INSERT_MODE_SIDE;
        }
        else if(insertTop){
            toSend = RoboticArmBlockEntity.INSERT_MODE_TOP;
        }
        else{
            toSend = RoboticArmBlockEntity.INSERT_MODE_NONE;
        }

        ClientPlayNetworking.send(new UpdateRoboticArmInsertModePacket(toSend));
    }

    private void getInsertMode(){
        int insertMode = handler.getSelectedInsertMode();

        if(insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE_TOP){
            insertSide = true;
            insertTop = true;
        }
        else if(insertMode == RoboticArmBlockEntity.INSERT_MODE_SIDE){
            insertSide = true;
            insertTop = false;
        }
        else if(insertMode == RoboticArmBlockEntity.INSERT_MODE_TOP){
            insertSide = false;
            insertTop = true;
        }
        else{
            insertSide = false;
            insertTop = false;
        }
    }
}
