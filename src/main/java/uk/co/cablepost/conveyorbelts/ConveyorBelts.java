package uk.co.cablepost.conveyorbelts;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.MapColor;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.resource.featuretoggle.FeatureSet;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.block.*;
import uk.co.cablepost.conveyorbelts.blockEntity.*;
import uk.co.cablepost.conveyorbelts.blockItem.ConveyorBeltBockItem;
import uk.co.cablepost.conveyorbelts.packets.CycleBeltElevationPacket;
import uk.co.cablepost.conveyorbelts.packets.UpdateFilterConveyorBeltOutputDirectionsModePacket;
import uk.co.cablepost.conveyorbelts.packets.UpdateInvertBeltPlacementPacket;
import uk.co.cablepost.conveyorbelts.packets.UpdateRoboticArmInsertModePacket;
import uk.co.cablepost.conveyorbelts.robotic_arm.base.RoboticArmScreenHandler;
import uk.co.cablepost.conveyorbelts.robotic_arm.filter.FilterRoboticArmScreenHandler;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.gold.GoldRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.iron.IronRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.netherite.NetheriteRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodFilterRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodFilterRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodRoboticArmBlock;
import uk.co.cablepost.conveyorbelts.robotic_arm.wood.WoodRoboticArmBlockEntity;
import uk.co.cablepost.conveyorbelts.screenHandler.ConveyorBeltScreenHandler;
import uk.co.cablepost.conveyorbelts.screenHandler.FilterConveyorBeltScreenHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ConveyorBelts implements ModInitializer {
    public static final String MOD_ID = "conveyorbelts";

    public static Map<UUID, Boolean> invertBeltPlacementOfPlayer = new HashMap<>();

    //BELT TRANSFER COOL DOWNS
    public static final int WOOD_TRANSFER_COOLDOWN = 30;
    public static final int IRON_TRANSFER_COOLDOWN = 4;
    public static final int GOLD_TRANSFER_COOLDOWN = 2;

    //BELT ENTITY MOVE SPEEDS
    public static final double WOOD_ENTITY_MOVE_SPEED = 0.011f;
    public static final double IRON_ENTITY_MOVE_SPEED = 0.108f;
    public static final double GOLD_ENTITY_MOVE_SPEED = 0.375f;

    //BELT MOVE ITEMS TO CENTER SPEED
    public static final int WOOD_MOVE_TO_CENTER_SPEED = 2;
    public static final int IRON_MOVE_TO_CENTER_SPEED = 5;
    public static final int GOLD_MOVE_TO_CENTER_SPEED = 12;

    //ROBOTIC ARM MAX PROGRESS (SPEEDS)
    public static final int WOOD_ROBOTIC_ARM_SPEED = 100;
    public static final int IRON_ROBOTIC_ARM_SPEED = 10;
    public static final int GOLD_ROBOTIC_ARM_SPEED = 6;
    public static final int NETHERITE_ROBOTIC_ARM_SPEED = 6;

    //BELT BLOCKS

    //==== REGULAR ====

    static FabricBlockSettings woodBlockSettings = FabricBlockSettings.create().mapColor(MapColor.OAK_TAN).sounds(BlockSoundGroup.WOOD).strength(0.8f);
    static FabricBlockSettings ironBlockSettings = FabricBlockSettings.create().mapColor(MapColor.GRAY).sounds(BlockSoundGroup.METAL).strength(0.8f);
    static FabricBlockSettings goldBlockSettings = FabricBlockSettings.create().mapColor(MapColor.GOLD).sounds(BlockSoundGroup.METAL).strength(0.8f);
    static FabricBlockSettings netheriteBlockSettings = FabricBlockSettings.create().mapColor(MapColor.DARK_RED).sounds(BlockSoundGroup.METAL).strength(0.8f);

    //wood
    public static final WoodConveyorBelt WOOD_CONVEYOR_BELT = new WoodConveyorBelt(FabricBlockSettings.copyOf(woodBlockSettings));
    public static final Identifier WOOD_CONVEYOR_BELT_IDENTIFIER = Identifier.of(MOD_ID, "wood_conveyor_belt");
    public static BlockEntityType<WoodConveyorBeltBlockEntity> WOOD_CONVEYOR_BELT_BLOCK_ENTITY;

    //iron
    public static IronConveyorBelt IRON_CONVEYOR_BELT = new IronConveyorBelt(FabricBlockSettings.copyOf(ironBlockSettings));
    public static final Identifier IRON_CONVEYOR_BELT_IDENTIFIER = Identifier.of(MOD_ID, "iron_conveyor_belt");
    public static BlockEntityType<IronConveyorBeltBlockEntity> IRON_CONVEYOR_BELT_BLOCK_ENTITY;

    //gold
    public static final GoldConveyorBelt GOLD_CONVEYOR_BELT = new GoldConveyorBelt(FabricBlockSettings.copyOf(goldBlockSettings));
    public static final Identifier GOLD_CONVEYOR_BELT_IDENTIFIER = Identifier.of(MOD_ID, "gold_conveyor_belt");
    public static BlockEntityType<GoldConveyorBeltBlockEntity> GOLD_CONVEYOR_BELT_BLOCK_ENTITY;

    //==== SLOPED UP ====

    //wood
    public static final WoodSlopedConveyorBeltUp WOOD_SLOPED_CONVEYOR_BELT_UP = new WoodSlopedConveyorBeltUp(FabricBlockSettings.copyOf(woodBlockSettings));
    public static final Identifier WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER = Identifier.of(MOD_ID, "wood_sloped_conveyor_belt_up");
    public static BlockEntityType<WoodSlopedConveyorBeltUpBlockEntity> WOOD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY;

    //iron
    public static final IronSlopedConveyorBeltUp IRON_SLOPED_CONVEYOR_BELT_UP = new IronSlopedConveyorBeltUp(FabricBlockSettings.copyOf(ironBlockSettings));
    public static final Identifier IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER = Identifier.of(MOD_ID, "iron_sloped_conveyor_belt_up");
    public static BlockEntityType<IronSlopedConveyorBeltUpBlockEntity> IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY;

    //gold
    public static final GoldSlopedConveyorBeltUp GOLD_SLOPED_CONVEYOR_BELT_UP = new GoldSlopedConveyorBeltUp(FabricBlockSettings.copyOf(goldBlockSettings));
    public static final Identifier GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER = Identifier.of(MOD_ID, "gold_sloped_conveyor_belt_up");
    public static BlockEntityType<GoldSlopedConveyorBeltUpBlockEntity> GOLD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY;

    //==== SLOPED DOWN ====

    //wood
    public static final WoodSlopedConveyorBeltDown WOOD_SLOPED_CONVEYOR_BELT_DOWN = new WoodSlopedConveyorBeltDown(FabricBlockSettings.copyOf(woodBlockSettings));
    public static final Identifier WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER = Identifier.of(MOD_ID, "wood_sloped_conveyor_belt_down");
    public static BlockEntityType<WoodSlopedConveyorBeltDownBlockEntity> WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY;

    //iron
    public static final IronSlopedConveyorBeltDown IRON_SLOPED_CONVEYOR_BELT_DOWN = new IronSlopedConveyorBeltDown(FabricBlockSettings.copyOf(ironBlockSettings));
    public static final Identifier IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER = Identifier.of(MOD_ID, "iron_sloped_conveyor_belt_down");
    public static BlockEntityType<IronSlopedConveyorBeltDownBlockEntity> IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY;

    //gold
    public static final GoldSlopedConveyorBeltDown GOLD_SLOPED_CONVEYOR_BELT_DOWN = new GoldSlopedConveyorBeltDown(FabricBlockSettings.copyOf(goldBlockSettings));
    public static final Identifier GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER = Identifier.of(MOD_ID, "gold_sloped_conveyor_belt_down");
    public static BlockEntityType<GoldSlopedConveyorBeltDownBlockEntity> GOLD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY;

    //==== FILTER ====

    //wood
    public static final WoodFilterConveyorBelt WOOD_FILTER_CONVEYOR_BELT = new WoodFilterConveyorBelt(FabricBlockSettings.copyOf(woodBlockSettings));
    public static final Identifier WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER = Identifier.of(MOD_ID, "wood_filter_conveyor_belt");
    public static BlockEntityType<WoodFilterConveyorBeltBlockEntity> WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY;

    //iron
    public static final IronFilterConveyorBelt IRON_FILTER_CONVEYOR_BELT = new IronFilterConveyorBelt(FabricBlockSettings.copyOf(ironBlockSettings));
    public static final Identifier IRON_FILTER_CONVEYOR_BELT_IDENTIFIER = Identifier.of(MOD_ID, "iron_filter_conveyor_belt");
    public static BlockEntityType<IronFilterConveyorBeltBlockEntity> IRON_FILTER_CONVEYOR_BELT_BLOCK_ENTITY;

    //gold
    public static final GoldFilterConveyorBelt GOLD_FILTER_CONVEYOR_BELT = new GoldFilterConveyorBelt(FabricBlockSettings.copyOf(goldBlockSettings));
    public static final Identifier GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER = Identifier.of(MOD_ID, "gold_filter_conveyor_belt");
    public static BlockEntityType<GoldFilterConveyorBeltBlockEntity> GOLD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY;

    //BELT SCREENS
    public static final ScreenHandlerType<ConveyorBeltScreenHandler> CONVEYOR_BELT_SCREEN_HANDLER = new ScreenHandlerType<>(ConveyorBeltScreenHandler::new, FeatureSet.empty());
    public static final ScreenHandlerType<FilterConveyorBeltScreenHandler> FILTER_CONVEYOR_BELT_SCREEN_HANDLER = new ScreenHandlerType<>(FilterConveyorBeltScreenHandler::new, FeatureSet.empty());

    //=== ROBOTIC ARMS ===
    //--- Wood ---
    public static final WoodRoboticArmBlock WOOD_ROBOTIC_ARM = new WoodRoboticArmBlock(FabricBlockSettings.copyOf(woodBlockSettings).strength(1.4f));
    public static final Identifier WOOD_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "wood_robotic_arm");
    public static BlockEntityType<WoodRoboticArmBlockEntity> WOOD_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Iron ---
    public static final IronRoboticArmBlock IRON_ROBOTIC_ARM = new IronRoboticArmBlock(FabricBlockSettings.copyOf(ironBlockSettings).strength(1.4f));
    public static final Identifier IRON_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "iron_robotic_arm");
    public static BlockEntityType<IronRoboticArmBlockEntity> IRON_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Gold ---
    public static final GoldRoboticArmBlock GOLD_ROBOTIC_ARM = new GoldRoboticArmBlock(FabricBlockSettings.copyOf(goldBlockSettings).strength(1.4f));
    public static final Identifier GOLD_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "gold_robotic_arm");
    public static BlockEntityType<GoldRoboticArmBlockEntity> GOLD_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Netherite ---
    public static final NetheriteRoboticArmBlock NETHERITE_ROBOTIC_ARM = new NetheriteRoboticArmBlock(FabricBlockSettings.copyOf(netheriteBlockSettings).strength(1.4f));
    public static final Identifier NETHERITE_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "netherite_robotic_arm");
    public static BlockEntityType<NetheriteRoboticArmBlockEntity> NETHERITE_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Wood Filter ---
    public static final WoodFilterRoboticArmBlock WOOD_FILTER_ROBOTIC_ARM = new WoodFilterRoboticArmBlock(FabricBlockSettings.copyOf(woodBlockSettings).strength(1.4f));
    public static final Identifier WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "wood_filter_robotic_arm");
    public static BlockEntityType<WoodFilterRoboticArmBlockEntity> WOOD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Iron Filter ---
    public static final IronFilterRoboticArmBlock IRON_FILTER_ROBOTIC_ARM = new IronFilterRoboticArmBlock(FabricBlockSettings.copyOf(ironBlockSettings).strength(1.4f));
    public static final Identifier IRON_FILTER_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "iron_filter_robotic_arm");
    public static BlockEntityType<IronFilterRoboticArmBlockEntity> IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Gold Filter ---
    public static final GoldFilterRoboticArmBlock GOLD_FILTER_ROBOTIC_ARM = new GoldFilterRoboticArmBlock(FabricBlockSettings.copyOf(goldBlockSettings).strength(1.4f));
    public static final Identifier GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "gold_filter_robotic_arm");
    public static BlockEntityType<GoldFilterRoboticArmBlockEntity> GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Netherite Filter ---
    public static final NetheriteFilterRoboticArmBlock NETHERITE_FILTER_ROBOTIC_ARM = new NetheriteFilterRoboticArmBlock(FabricBlockSettings.copyOf(netheriteBlockSettings).strength(1.4f));
    public static final Identifier NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER = Identifier.of(MOD_ID, "netherite_filter_robotic_arm");
    public static BlockEntityType<NetheriteFilterRoboticArmBlockEntity> NETHERITE_FILTER_ROBOTIC_ARM_BLOCK_ENTITY;

    //--- Screens ---
    public static final ScreenHandlerType<RoboticArmScreenHandler> ROBOTIC_ARM_SCREEN_HANDLER = new ScreenHandlerType<>(RoboticArmScreenHandler::new, FeatureSet.empty());
    public static final ScreenHandlerType<FilterRoboticArmScreenHandler> FILTER_ROBOTIC_ARM_SCREEN_HANDLER = new ScreenHandlerType<>(FilterRoboticArmScreenHandler::new, FeatureSet.empty());

    public static final ItemGroup ITEM_GROUP = FabricItemGroup.builder()
            .icon(() -> new ItemStack(IRON_CONVEYOR_BELT))
            .displayName(Text.translatable("itemGroup.conveyorbelts.items"))
            .entries((context, entries) -> {
                entries.add(WOOD_CONVEYOR_BELT);
                entries.add(WOOD_SLOPED_CONVEYOR_BELT_UP);
                entries.add(WOOD_SLOPED_CONVEYOR_BELT_DOWN);
                entries.add(WOOD_FILTER_CONVEYOR_BELT);

                entries.add(IRON_CONVEYOR_BELT);
                entries.add(IRON_SLOPED_CONVEYOR_BELT_UP);
                entries.add(IRON_SLOPED_CONVEYOR_BELT_DOWN);
                entries.add(IRON_FILTER_CONVEYOR_BELT);

                entries.add(GOLD_CONVEYOR_BELT);
                entries.add(GOLD_SLOPED_CONVEYOR_BELT_UP);
                entries.add(GOLD_SLOPED_CONVEYOR_BELT_DOWN);
                entries.add(GOLD_FILTER_CONVEYOR_BELT);

                entries.add(WOOD_ROBOTIC_ARM);
                entries.add(WOOD_FILTER_ROBOTIC_ARM);

                entries.add(IRON_ROBOTIC_ARM);
                entries.add(IRON_FILTER_ROBOTIC_ARM);

                entries.add(GOLD_ROBOTIC_ARM);
                entries.add(GOLD_FILTER_ROBOTIC_ARM);

                entries.add(NETHERITE_ROBOTIC_ARM);
                entries.add(NETHERITE_FILTER_ROBOTIC_ARM);
            })
            .build()
    ;



    @Override
    public void onInitialize() {

        //==== REGULAR ====

        //wood

        WOOD_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                WOOD_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodConveyorBeltBlockEntity::new,
                        WOOD_CONVEYOR_BELT
                ).build(null)
        );

        Registry.register(Registries.BLOCK, WOOD_CONVEYOR_BELT_IDENTIFIER, WOOD_CONVEYOR_BELT);
        Registry.register(Registries.ITEM, WOOD_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(WOOD_CONVEYOR_BELT, new Item.Settings()));

        //iron

        IRON_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronConveyorBeltBlockEntity::new,
                        IRON_CONVEYOR_BELT
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_CONVEYOR_BELT_IDENTIFIER, IRON_CONVEYOR_BELT);
        Registry.register(Registries.ITEM, IRON_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(IRON_CONVEYOR_BELT, new Item.Settings()));

        //gold

        GOLD_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldConveyorBeltBlockEntity::new,
                        GOLD_CONVEYOR_BELT
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_CONVEYOR_BELT_IDENTIFIER, GOLD_CONVEYOR_BELT);
        Registry.register(Registries.ITEM, GOLD_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(GOLD_CONVEYOR_BELT, new Item.Settings()));


        //==== SLOPED UP ====

        //wood

        WOOD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodSlopedConveyorBeltUpBlockEntity::new,
                        WOOD_SLOPED_CONVEYOR_BELT_UP
                ).build(null)
        );
        Registry.register(Registries.BLOCK, WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, WOOD_SLOPED_CONVEYOR_BELT_UP);
        Registry.register(Registries.ITEM, WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, new ConveyorBeltBockItem(WOOD_SLOPED_CONVEYOR_BELT_UP, new Item.Settings()));

        //iron

        IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronSlopedConveyorBeltUpBlockEntity::new,
                        IRON_SLOPED_CONVEYOR_BELT_UP
                ).build(null)
        );
        Registry.register(Registries.BLOCK, IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, IRON_SLOPED_CONVEYOR_BELT_UP);
        Registry.register(Registries.ITEM, IRON_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, new ConveyorBeltBockItem(IRON_SLOPED_CONVEYOR_BELT_UP, new Item.Settings()));

        //gold

        GOLD_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldSlopedConveyorBeltUpBlockEntity::new,
                        GOLD_SLOPED_CONVEYOR_BELT_UP
                ).build(null)
        );
        Registry.register(Registries.BLOCK, GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, GOLD_SLOPED_CONVEYOR_BELT_UP);
        Registry.register(Registries.ITEM, GOLD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER, new ConveyorBeltBockItem(GOLD_SLOPED_CONVEYOR_BELT_UP, new Item.Settings()));

        //==== SLOPED DOWN ====

        //wood

        WOOD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodSlopedConveyorBeltDownBlockEntity::new,
                        WOOD_SLOPED_CONVEYOR_BELT_DOWN
                ).build(null)
        );
        Registry.register(Registries.BLOCK, WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, WOOD_SLOPED_CONVEYOR_BELT_DOWN);
        Registry.register(Registries.ITEM, WOOD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, new ConveyorBeltBockItem(WOOD_SLOPED_CONVEYOR_BELT_DOWN, new Item.Settings()));

        //iron

        IRON_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronSlopedConveyorBeltDownBlockEntity::new,
                        IRON_SLOPED_CONVEYOR_BELT_DOWN
                ).build(null)
        );
        Registry.register(Registries.BLOCK, IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, IRON_SLOPED_CONVEYOR_BELT_DOWN);
        Registry.register(Registries.ITEM, IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, new ConveyorBeltBockItem(IRON_SLOPED_CONVEYOR_BELT_DOWN, new Item.Settings()));

        //gold

        GOLD_SLOPED_CONVEYOR_BELT_DOWN_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldSlopedConveyorBeltDownBlockEntity::new,
                        GOLD_SLOPED_CONVEYOR_BELT_DOWN
                ).build(null)
        );
        Registry.register(Registries.BLOCK, GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, GOLD_SLOPED_CONVEYOR_BELT_DOWN);
        Registry.register(Registries.ITEM, GOLD_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER, new ConveyorBeltBockItem(GOLD_SLOPED_CONVEYOR_BELT_DOWN, new Item.Settings()));

        //==== FILTER ====

        //wood

        WOOD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodFilterConveyorBeltBlockEntity::new,
                        WOOD_FILTER_CONVEYOR_BELT
                ).build(null)
        );
        Registry.register(Registries.BLOCK, WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER, WOOD_FILTER_CONVEYOR_BELT);
        Registry.register(Registries.ITEM, WOOD_FILTER_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(WOOD_FILTER_CONVEYOR_BELT, new Item.Settings()));

        //iron

        IRON_FILTER_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_FILTER_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronFilterConveyorBeltBlockEntity::new,
                        IRON_FILTER_CONVEYOR_BELT
                ).build(null)
        );
        Registry.register(Registries.BLOCK, IRON_FILTER_CONVEYOR_BELT_IDENTIFIER, IRON_FILTER_CONVEYOR_BELT);
        Registry.register(Registries.ITEM, IRON_FILTER_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(IRON_FILTER_CONVEYOR_BELT, new Item.Settings()));

        //gold

        GOLD_FILTER_CONVEYOR_BELT_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldFilterConveyorBeltBlockEntity::new,
                        GOLD_FILTER_CONVEYOR_BELT
                ).build(null)
        );
        Registry.register(Registries.BLOCK, GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER, GOLD_FILTER_CONVEYOR_BELT);
        Registry.register(Registries.ITEM, GOLD_FILTER_CONVEYOR_BELT_IDENTIFIER, new ConveyorBeltBockItem(GOLD_FILTER_CONVEYOR_BELT, new Item.Settings()));

        //=== ROBOTIC ARM ===

        //Wood
        WOOD_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                WOOD_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodRoboticArmBlockEntity::new,
                        WOOD_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, WOOD_ROBOTIC_ARM_IDENTIFIER, WOOD_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, WOOD_ROBOTIC_ARM_IDENTIFIER, new BlockItem(WOOD_ROBOTIC_ARM, new Item.Settings()));

        //Iron
        IRON_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronRoboticArmBlockEntity::new,
                        IRON_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_ROBOTIC_ARM_IDENTIFIER, IRON_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, IRON_ROBOTIC_ARM_IDENTIFIER, new BlockItem(IRON_ROBOTIC_ARM, new Item.Settings()));

        //Gold
        GOLD_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldRoboticArmBlockEntity::new,
                        GOLD_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_ROBOTIC_ARM_IDENTIFIER, GOLD_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, GOLD_ROBOTIC_ARM_IDENTIFIER, new BlockItem(GOLD_ROBOTIC_ARM, new Item.Settings()));

        //Netherite
        NETHERITE_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                NETHERITE_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        NetheriteRoboticArmBlockEntity::new,
                        NETHERITE_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, NETHERITE_ROBOTIC_ARM_IDENTIFIER, NETHERITE_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, NETHERITE_ROBOTIC_ARM_IDENTIFIER, new BlockItem(NETHERITE_ROBOTIC_ARM, new Item.Settings()));

        //Wood Filter
        WOOD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        WoodFilterRoboticArmBlockEntity::new,
                        WOOD_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER, WOOD_FILTER_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, WOOD_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(WOOD_FILTER_ROBOTIC_ARM, new Item.Settings()));

        //Iron Filter
        IRON_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                IRON_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        IronFilterRoboticArmBlockEntity::new,
                        IRON_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, IRON_FILTER_ROBOTIC_ARM_IDENTIFIER, IRON_FILTER_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, IRON_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(IRON_FILTER_ROBOTIC_ARM, new Item.Settings()));

        //Gold Filter
        GOLD_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        GoldFilterRoboticArmBlockEntity::new,
                        GOLD_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER, GOLD_FILTER_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, GOLD_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(GOLD_FILTER_ROBOTIC_ARM, new Item.Settings()));

        //Netherite Filter
        NETHERITE_FILTER_ROBOTIC_ARM_BLOCK_ENTITY = Registry.register(
                Registries.BLOCK_ENTITY_TYPE,
                NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER,
                FabricBlockEntityTypeBuilder.create(
                        NetheriteFilterRoboticArmBlockEntity::new,
                        NETHERITE_FILTER_ROBOTIC_ARM
                ).build(null)
        );

        Registry.register(Registries.BLOCK, NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER, NETHERITE_FILTER_ROBOTIC_ARM);
        Registry.register(Registries.ITEM, NETHERITE_FILTER_ROBOTIC_ARM_IDENTIFIER, new BlockItem(NETHERITE_FILTER_ROBOTIC_ARM, new Item.Settings()));

        //=== PACKETS ===

        PayloadTypeRegistry.playC2S().register(UpdateInvertBeltPlacementPacket.PACKET_ID, UpdateInvertBeltPlacementPacket.CODEC);

        ServerPlayNetworking.registerGlobalReceiver(UpdateInvertBeltPlacementPacket.PACKET_ID, (payload, context) -> {
            context.server().executeSync(() -> {
                invertBeltPlacementOfPlayer.put(context.player().getUuid(), payload.invert());
                if(payload.sendChatMessage()) {
                    if (payload.invert()) {
                        context.player().sendMessage(Text.translatable("text.conveyorbelts.belt_will_place_away.message").formatted(Formatting.GRAY), false);
                    } else {
                        context.player().sendMessage(Text.translatable("text.conveyorbelts.belt_will_place_towards.message").formatted(Formatting.GRAY), false);
                    }
                }
            });
        });

        PayloadTypeRegistry.playC2S().register(CycleBeltElevationPacket.PACKET_ID, CycleBeltElevationPacket.CODEC);

        ServerPlayNetworking.registerGlobalReceiver(CycleBeltElevationPacket.PACKET_ID, (payload, context) -> {
            context.server().executeSync(() -> {
                ItemStack stack = context.player().getStackInHand(Hand.MAIN_HAND);

                if(stack.getItem() instanceof ConveyorBeltBockItem i){
                    if(i.getBlock() instanceof ConveyorBelt b){
                        Identifier next = b.getNextCycleBeltType();
                        if(next != null) {
                            context.player().setStackInHand(Hand.MAIN_HAND, new ItemStack(Registries.ITEM.get(next), stack.getCount()));
                        }
                    }
                }
            });
        });

        PayloadTypeRegistry.playC2S().register(UpdateRoboticArmInsertModePacket.PACKET_ID, UpdateRoboticArmInsertModePacket.CODEC);

        ServerPlayNetworking.registerGlobalReceiver(UpdateRoboticArmInsertModePacket.PACKET_ID, (payload, context) -> {
            context.server().executeSync(() -> {
                if (!context.player().isSpectator() && context.player().currentScreenHandler instanceof RoboticArmScreenHandler screenHandler) {
                    screenHandler.setSelectedInsertMode(payload.insertMode());
                }
            });
        });

        PayloadTypeRegistry.playC2S().register(UpdateFilterConveyorBeltOutputDirectionsModePacket.PACKET_ID, UpdateFilterConveyorBeltOutputDirectionsModePacket.CODEC);

        ServerPlayNetworking.registerGlobalReceiver(UpdateFilterConveyorBeltOutputDirectionsModePacket.PACKET_ID, (payload, context) -> {
            context.server().executeSync(() -> {
                if (!context.player().isSpectator() && context.player().currentScreenHandler instanceof FilterConveyorBeltScreenHandler screenHandler) {
                    screenHandler.setOutputDirectionsMode(payload.outputDirectionsMode());
                }
            });
        });

        Registry.register(Registries.SCREEN_HANDLER, Identifier.of(MOD_ID, "conveyor_belt_screen"), CONVEYOR_BELT_SCREEN_HANDLER);
        Registry.register(Registries.SCREEN_HANDLER, Identifier.of(MOD_ID, "filter_conveyor_belt_screen"), FILTER_CONVEYOR_BELT_SCREEN_HANDLER);

        Registry.register(Registries.SCREEN_HANDLER, Identifier.of(MOD_ID, "robotic_arm_screen"), ROBOTIC_ARM_SCREEN_HANDLER);
        Registry.register(Registries.SCREEN_HANDLER, Identifier.of(MOD_ID, "filter_robotic_arm_screen"), FILTER_ROBOTIC_ARM_SCREEN_HANDLER);

        Registry.register(Registries.ITEM_GROUP, Identifier.of(MOD_ID, "main_creative_inventory_tab"), ITEM_GROUP);
    }
}
