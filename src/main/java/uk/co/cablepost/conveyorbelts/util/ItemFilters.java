package uk.co.cablepost.conveyorbelts.util;

import net.minecraft.item.ItemStack;

public class ItemFilters {
    public static boolean Filter(ItemStack filterStackParam, ItemStack inputStackParam){
        return ItemStack.areItemsAndComponentsEqual(filterStackParam, inputStackParam);
    }
}
