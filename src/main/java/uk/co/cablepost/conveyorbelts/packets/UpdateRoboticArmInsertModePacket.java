package uk.co.cablepost.conveyorbelts.packets;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public record UpdateRoboticArmInsertModePacket(
    int insertMode
) implements CustomPayload {
    public static final Identifier IDENTIFIER = Identifier.of(ConveyorBelts.MOD_ID, "update_robotic_arm_insert_mode");
    public static final CustomPayload.Id<UpdateRoboticArmInsertModePacket> PACKET_ID = new CustomPayload.Id<>(IDENTIFIER);

    public static final PacketCodec<RegistryByteBuf, UpdateRoboticArmInsertModePacket> CODEC = PacketCodec.tuple(
        PacketCodecs.INTEGER, UpdateRoboticArmInsertModePacket::insertMode,
        UpdateRoboticArmInsertModePacket::new
    );

    @Override
    public CustomPayload.Id<? extends CustomPayload> getId() {
        return PACKET_ID;
    }
}