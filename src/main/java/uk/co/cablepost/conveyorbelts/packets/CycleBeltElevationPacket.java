package uk.co.cablepost.conveyorbelts.packets;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;


public record CycleBeltElevationPacket(
    boolean dud
) implements CustomPayload {
    public static final Identifier IDENTIFIER = Identifier.of(ConveyorBelts.MOD_ID, "cycle_belt_elevation");
    public static final CustomPayload.Id<CycleBeltElevationPacket> PACKET_ID = new CustomPayload.Id<>(IDENTIFIER);

    public static final PacketCodec<RegistryByteBuf, CycleBeltElevationPacket> CODEC = PacketCodec.tuple(
        PacketCodecs.BOOL, CycleBeltElevationPacket::dud,
        CycleBeltElevationPacket::new
    );

    @Override
    public CustomPayload.Id<? extends CustomPayload> getId() {
        return PACKET_ID;
    }
}