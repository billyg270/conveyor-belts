package uk.co.cablepost.conveyorbelts.packets;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public record UpdateInvertBeltPlacementPacket(
    boolean invert,
    boolean sendChatMessage
) implements CustomPayload {
    public static final Identifier IDENTIFIER = Identifier.of(ConveyorBelts.MOD_ID, "update_invert_belt_placement");
    public static final CustomPayload.Id<UpdateInvertBeltPlacementPacket> PACKET_ID = new CustomPayload.Id<>(IDENTIFIER);

    public static final PacketCodec<RegistryByteBuf, UpdateInvertBeltPlacementPacket> CODEC = PacketCodec.tuple(
        PacketCodecs.BOOL, UpdateInvertBeltPlacementPacket::invert,
        PacketCodecs.BOOL, UpdateInvertBeltPlacementPacket::sendChatMessage,
        UpdateInvertBeltPlacementPacket::new
    );

    @Override
    public CustomPayload.Id<? extends CustomPayload> getId() {
        return PACKET_ID;
    }
}
