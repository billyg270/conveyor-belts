package uk.co.cablepost.conveyorbelts.packets;

import net.minecraft.network.RegistryByteBuf;
import net.minecraft.network.codec.PacketCodec;
import net.minecraft.network.codec.PacketCodecs;
import net.minecraft.network.packet.CustomPayload;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;

public record UpdateFilterConveyorBeltOutputDirectionsModePacket(
    int outputDirectionsMode
) implements CustomPayload {
    public static final Identifier IDENTIFIER = Identifier.of(ConveyorBelts.MOD_ID, "update_filter_conveyor_belt_output_directions_mode");
    public static final CustomPayload.Id<UpdateFilterConveyorBeltOutputDirectionsModePacket> PACKET_ID = new CustomPayload.Id<>(IDENTIFIER);

    public static final PacketCodec<RegistryByteBuf, UpdateFilterConveyorBeltOutputDirectionsModePacket> CODEC = PacketCodec.tuple(
        PacketCodecs.INTEGER, UpdateFilterConveyorBeltOutputDirectionsModePacket::outputDirectionsMode,
        UpdateFilterConveyorBeltOutputDirectionsModePacket::new
    );

    @Override
    public CustomPayload.Id<? extends CustomPayload> getId() {
        return PACKET_ID;
    }
}