package uk.co.cablepost.conveyorbelts.screen;

import com.mojang.blaze3d.systems.RenderSystem;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.packets.UpdateFilterConveyorBeltOutputDirectionsModePacket;
import uk.co.cablepost.conveyorbelts.screenHandler.FilterConveyorBeltScreenHandler;

public class FilterConveyorBeltScreen extends HandledScreen<FilterConveyorBeltScreenHandler> {
    //A path to the gui texture. In this example we use the texture from the dispenser
    private static final Identifier TEXTURE = Identifier.of(ConveyorBelts.MOD_ID, "textures/gui/container/filter_conveyor_belt.png");
    private static final Identifier SETTINGS_TEXTURE = Identifier.of(ConveyorBelts.MOD_ID, "textures/gui/container/filter_conveyor_belt_settings.png");

    public boolean settingsScreen = false;
    public int outputDirectionsMode = 0;

    public FilterConveyorBeltScreen(FilterConveyorBeltScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
    }

    @Override
    protected void drawBackground(DrawContext context, float delta, int mouseX, int mouseY) {
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        context.drawTexture(settingsScreen ? SETTINGS_TEXTURE : TEXTURE, x, y, 0, 0, backgroundWidth, backgroundHeight);

        outputDirectionsMode = handler.getOutputDirectionsMode();

        if(settingsScreen){
            context.drawText(this.textRenderer, Text.of("Output A"), x + 29, y + 28, 4210752, false);
            context.drawText(this.textRenderer, Text.of("Left"), x + 38, y + 39, 4210752, false);
            context.drawText(this.textRenderer, Text.of("Back"), x + 38, y + 51, 4210752, false);

            if(outputDirectionsMode == FilterConveyorBeltBlockEntity.OUTPUT_MODE_LEFT_FRONT){
                context.drawText(this.textRenderer, Text.of("x"), x + 30, y + 51, 0xffffff, false);
            }
            else {
                context.drawText(this.textRenderer, Text.of("x"), x + 30, y + 39, 0xffffff, false);
            }

            context.drawText(this.textRenderer, Text.of("Output B"), x + 106, y + 28, 4210752, false);
            context.drawText(this.textRenderer, Text.of("Right"), x + 115, y + 39, 4210752, false);
            context.drawText(this.textRenderer, Text.of("Back"), x + 115, y + 51, 4210752, false);

            if(outputDirectionsMode == FilterConveyorBeltBlockEntity.OUTPUT_MODE_RIGHT_FRONT){
                context.drawText(this.textRenderer, Text.of("x"), x + 107, y + 51, 0xffffff, false);
            }
            else {
                context.drawText(this.textRenderer, Text.of("x"), x + 107, y + 39, 0xffffff, false);
            }
        }
    }

    @Override
    public void render(DrawContext context, int mouseX, int mouseY, float delta) {
        if(settingsScreen) {
            int i = this.x;
            int j = this.y;
            this.drawBackground(context, delta, mouseX, mouseY);
            RenderSystem.disableDepthTest();
            MatrixStack matrixStack = context.getMatrices();
            matrixStack.push();
            matrixStack.translate(i, j, 0.0);
            RenderSystem.applyModelViewMatrix();
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            this.focusedSlot = null;
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            context.drawText(this.textRenderer, this.title, this.titleX, this.titleY, 4210752, false);

            matrixStack.pop();
            RenderSystem.applyModelViewMatrix();
            RenderSystem.enableDepthTest();
        }
        else{
            super.render(context, mouseX, mouseY, delta);
            this.drawMouseoverTooltip(context, mouseX, mouseY);
        }
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        if (client == null || client.player == null || client.player.isSpectator()) {
            return super.mouseClicked(mouseX, mouseY, button);
        }

        int x = (this.width - this.backgroundWidth) / 2;
        int y = (this.height - this.backgroundHeight) / 2;

        double mx = mouseX - x;
        double my = mouseY - y;

        if(mx >= 158 && mx <= 168 && my >= 7 && my <= 18){
            //switch screens
            client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
            settingsScreen = !settingsScreen;
        }

        if(settingsScreen){
            if(mx >= 29 && mx <= 36){
                if(my >= 40 && my <= 47){
                    //A - left side

                    if(outputDirectionsMode == FilterConveyorBeltBlockEntity.OUTPUT_MODE_LEFT_FRONT){
                        outputDirectionsMode = FilterConveyorBeltBlockEntity.OUTPUT_MODE_NORMAL;

                        client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                        sendOutputDirectionsMode();
                        return true;
                    }
                }
                if(my >= 52 && my <= 59){
                    //A - back

                    if(outputDirectionsMode != FilterConveyorBeltBlockEntity.OUTPUT_MODE_LEFT_FRONT){
                        outputDirectionsMode = FilterConveyorBeltBlockEntity.OUTPUT_MODE_LEFT_FRONT;

                        client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                        sendOutputDirectionsMode();
                        return true;
                    }
                }
            }

            if(mx >= 106 && mx <= 113){
                if(my >= 40 && my <= 47){
                    //B - right side

                    if(outputDirectionsMode == FilterConveyorBeltBlockEntity.OUTPUT_MODE_RIGHT_FRONT){
                        outputDirectionsMode = FilterConveyorBeltBlockEntity.OUTPUT_MODE_NORMAL;

                        client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                        sendOutputDirectionsMode();
                        return true;
                    }
                }
                if(my >= 52 && my <= 59){
                    //B - back

                    if(outputDirectionsMode != FilterConveyorBeltBlockEntity.OUTPUT_MODE_RIGHT_FRONT){
                        outputDirectionsMode = FilterConveyorBeltBlockEntity.OUTPUT_MODE_RIGHT_FRONT;

                        client.player.playSound(SoundEvents.UI_BUTTON_CLICK.value(), 0.3f, 1.0f);
                        sendOutputDirectionsMode();
                        return true;
                    }
                }
            }


            return false;
        }
        else{
            return super.mouseClicked(mouseX, mouseY, button);
        }
    }

    private void sendOutputDirectionsMode(){
        ClientPlayNetworking.send(new UpdateFilterConveyorBeltOutputDirectionsModePacket(outputDirectionsMode));
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}