package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import uk.co.cablepost.conveyorbelts.blockEntity.FilterConveyorBeltBlockEntity;

public abstract class FilterConveyorBelt extends ConveyorBelt {
    public static final IntProperty OUTPUT_MODE = IntProperty.of("outputmode", 0, 2);

    public FilterConveyorBelt(Settings settings) {
        super(settings, true);
        this.setDefaultState(
                this.stateManager.getDefaultState()
                        .with(ENABLED, true)
                        .with(FACING, Direction.NORTH)
                        .with(OUTPUT_MODE, 0)
        );
    }

    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(
                ENABLED,
                FACING,
                OUTPUT_MODE
        );
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return VoxelShapes.cuboid(0.0f, 0.0f, 0.0f, 1.0f, 0.75f, 1.0f);
    }

    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        FilterConveyorBeltBlockEntity blockEntity = (FilterConveyorBeltBlockEntity) world.getBlockEntity(pos);

        int i = 0;
        float f = 0.0F;

        for(int j = 0; j < 4; ++j) {
            assert blockEntity != null;
            ItemStack itemStack = blockEntity.getStack(j);
            if (!itemStack.isEmpty()) {
                f += (float)itemStack.getCount() / (float)Math.min(blockEntity.getMaxCountPerStack(), itemStack.getMaxCount());
                ++i;
            }
        }

        f /= (float)4;
        return MathHelper.floor(f * 14.0F) + (i > 0 ? 1 : 0);

        //return ScreenHandler.calculateComparatorOutput(world.getBlockEntity(pos));
    }

    @Override
    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
    }

    @Override
    public Identifier getNextCycleBeltType() {
        return null;
    }
}
