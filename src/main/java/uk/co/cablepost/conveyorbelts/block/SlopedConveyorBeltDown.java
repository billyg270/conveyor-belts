package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.enums.DoubleBlockHalf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.SlopedConveyorBeltDownBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.SlopedConveyorBeltUpBlockEntity;
import uk.co.cablepost.conveyorbelts.client.ConveyorBeltsClient;

import java.util.Objects;

public abstract class SlopedConveyorBeltDown extends SlopedConveyorBeltUp {
    public SlopedConveyorBeltDown(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return getOutlineShape2(state, state.get(FACING).getOpposite());
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        Direction side = ctx.getSide();
        PlayerEntity player = ctx.getPlayer();

        if(player == null){
            return this.getDefaultState();
        }

        boolean sneaking = false;//player.isSneaking();
        if (ctx.getWorld().isClient) {
            sneaking = ConveyorBeltsClient.invertBeltPlacement;
        }
        else{
            Boolean getInv = ConveyorBelts.invertBeltPlacementOfPlayer.get(player.getUuid());
            sneaking = getInv != null && getInv;
        }

        if((side.getOffsetX() != 0 || side.getOffsetZ() != 0) && side.getOffsetY() == 0){
            if(sneaking) {
                return super.getPlacementState(ctx).with(HALF, DoubleBlockHalf.LOWER);
            }
            else{
                return super.getPlacementState(ctx).with(HALF, DoubleBlockHalf.UPPER);
            }
        }

        return super.getPlacementState(ctx);
    }
}
