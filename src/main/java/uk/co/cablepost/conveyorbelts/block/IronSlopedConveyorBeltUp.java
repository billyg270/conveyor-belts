package uk.co.cablepost.conveyorbelts.block;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.IronConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.blockEntity.IronSlopedConveyorBeltUpBlockEntity;

public class IronSlopedConveyorBeltUp extends SlopedConveyorBeltUp {
    public IronSlopedConveyorBeltUp(AbstractBlock.Settings settings) {
        super(settings);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new IronSlopedConveyorBeltUpBlockEntity(pos, state);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return createCodec(IronSlopedConveyorBeltUp::new);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? validateTicker(type, ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY, IronSlopedConveyorBeltUpBlockEntity::clientTick) : validateTicker(type, ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_UP_BLOCK_ENTITY, IronSlopedConveyorBeltUpBlockEntity::serverTick);
    }

    @Override
    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
        vector = vector.multiply(ConveyorBelts.IRON_ENTITY_MOVE_SPEED);
        moveEntityOn2(vector, livingEntity);
    }

    public Identifier getNextCycleBeltType(){
        return ConveyorBelts.IRON_SLOPED_CONVEYOR_BELT_DOWN_IDENTIFIER;
    }
}
