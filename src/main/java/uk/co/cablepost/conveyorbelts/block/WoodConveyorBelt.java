package uk.co.cablepost.conveyorbelts.block;

import com.mojang.serialization.MapCodec;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.WoodConveyorBeltBlockEntity;

public class WoodConveyorBelt extends ConveyorBelt{

    public WoodConveyorBelt(AbstractBlock.Settings settings) {
        super(settings, false);
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new WoodConveyorBeltBlockEntity(pos, state);
    }

    @Override
    protected MapCodec<? extends BlockWithEntity> getCodec() {
        return createCodec(WoodConveyorBelt::new);
    }

    @Nullable
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? validateTicker(type, ConveyorBelts.WOOD_CONVEYOR_BELT_BLOCK_ENTITY, WoodConveyorBeltBlockEntity::clientTick) : validateTicker(type, ConveyorBelts.WOOD_CONVEYOR_BELT_BLOCK_ENTITY, WoodConveyorBeltBlockEntity::serverTick);
    }

    @Override
    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
        vector = vector.multiply(ConveyorBelts.WOOD_ENTITY_MOVE_SPEED);
        moveEntityOn2(vector, livingEntity);
    }

    public Identifier getNextCycleBeltType(){
        return ConveyorBelts.WOOD_SLOPED_CONVEYOR_BELT_UP_IDENTIFIER;
    }
}
