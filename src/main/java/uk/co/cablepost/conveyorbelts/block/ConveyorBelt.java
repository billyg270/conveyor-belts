package uk.co.cablepost.conveyorbelts.block;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.DirectionProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.text.Text;
import net.minecraft.util.*;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.conveyorbelts.ConveyorBelts;
import uk.co.cablepost.conveyorbelts.blockEntity.ConveyorBeltBlockEntity;
import uk.co.cablepost.conveyorbelts.blockItem.ConveyorBeltBockItem;
import uk.co.cablepost.conveyorbelts.client.ConveyorBeltsClient;

public abstract class ConveyorBelt extends BlockWithEntity {
    public static final BooleanProperty ENABLED;
    public static final DirectionProperty FACING;

    static {
        ENABLED = Properties.ENABLED;
        FACING = Properties.HORIZONTAL_FACING;
    }
    public ConveyorBelt(Settings settings, boolean skipSetDefaultState) {
        super(settings);

        if(!skipSetDefaultState){
            return;
        }
        this.setDefaultState(
                this.stateManager.getDefaultState()
                        .with(ENABLED, true)
                        .with(FACING, Direction.NORTH)
        );
    }

    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        builder.add(
                ENABLED,
                FACING
        );
    }

    public BlockState getPlacementState(ItemPlacementContext ctx) {
        Direction facing = ctx.getHorizontalPlayerFacing().getOpposite();
        PlayerEntity player = ctx.getPlayer();

        if(player == null){
            return this.getDefaultState();
        }

        boolean sneaking = false;//player.isSneaking();
        if (ctx.getWorld().isClient) {
            sneaking = ConveyorBeltsClient.invertBeltPlacement;
        }
        else{
            Boolean getInv = ConveyorBelts.invertBeltPlacementOfPlayer.get(player.getUuid());
            sneaking = getInv != null && getInv;
        }

        if(sneaking){
            facing = facing.getOpposite();

            /*
            if(side.getOffsetY() == 0){
                facing = side;
            }
            */
        }

        return this.getDefaultState()
                .with(ENABLED, !ctx.getWorld().isReceivingRedstonePower(ctx.getBlockPos()))
                .with(FACING, facing)
        ;
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        return VoxelShapes.cuboid(0.0f, 0.0f, 0.0f, 1.0f, 0.375f, 1.0f);
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity instanceof ConveyorBeltBlockEntity conveyorBeltBlockEntity) {
                //---

                //ItemScatterer.spawn(world, pos, (ConveyorBeltBlockEntity)blockEntity);//Replace with the below so does not drop filter items
                for(int i = 0; i < conveyorBeltBlockEntity.size(); ++i) {
                    ItemStack stack = conveyorBeltBlockEntity.getStack(i);

                    ItemScatterer.spawn(world, pos.getX(), pos.getY(), pos.getZ(), stack);
                }

                //---

                // update comparators
                world.updateComparators(pos,this);
            }
            super.onStateReplaced(state, world, pos, newState, moved);
        }
    }

    @Override
    public boolean hasComparatorOutput(BlockState state) {
        return true;
    }

    @Override
    public int getComparatorOutput(BlockState state, World world, BlockPos pos) {
        return ScreenHandler.calculateComparatorOutput(world.getBlockEntity(pos));
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        //With inheriting from BlockWithEntity this defaults to INVISIBLE, so we need to change that!
        return BlockRenderType.MODEL;
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, BlockHitResult hit) {
        if(player.getStackInHand(Hand.MAIN_HAND).getItem() instanceof ConveyorBeltBockItem || player.getStackInHand(Hand.OFF_HAND).getItem() instanceof ConveyorBeltBockItem){
            //Holding a belt
            player.sendMessage(Text.translatable("text.conveyorbelts.belt_place_on_belt.message").formatted(Formatting.GRAY), true);

            return ActionResult.PASS;//Allow placing of belt on this without shifting
        }

        if (!world.isClient) {
            //This will call the createScreenHandlerFactory method from BlockWithEntity, which will return our blockEntity casted to
            //a namedScreenHandlerFactory. If your block class does not extend BlockWithEntity, it needs to implement createScreenHandlerFactory.
            NamedScreenHandlerFactory screenHandlerFactory = state.createScreenHandlerFactory(world, pos);

            if (screenHandlerFactory != null) {
                //With this call the server will request the client to open the appropriate Screenhandler
                player.openHandledScreen(screenHandlerFactory);
            }
        }

        return ActionResult.SUCCESS;
    }

    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean notify) {
        if (!oldState.isOf(state.getBlock())) {
            this.updateEnabled(world, pos, state);
        }
    }

    public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos fromPos, boolean notify) {
        this.updateEnabled(world, pos, state);
    }

    private void updateEnabled(World world, BlockPos pos, BlockState state) {
        boolean bl = !world.isReceivingRedstonePower(pos);
        if (bl != state.get(ENABLED)) {
            world.setBlockState(pos, state.with(ENABLED, bl), 2);
        }
    }

    @Override
    public void onSteppedOn(World world, BlockPos pos, BlockState state, Entity entity) {

        if(!state.get(ConveyorBelt.ENABLED)){
            return;
        }

        if(entity instanceof LivingEntity){
            LivingEntity le = (LivingEntity)entity;
            if(!le.isSneaking()) {
                Direction direction = state.get(ConveyorBelt.FACING);
                Vec3i vectorI = direction.getVector();
                Vec3d vector = new Vec3d(vectorI.getX(), vectorI.getY(), vectorI.getZ());
                moveEntityOn(vector, le);
            }
        }

        super.onSteppedOn(world, pos, state, entity);
    }

    public void moveEntityOn(Vec3d vector, LivingEntity livingEntity){
        vector = vector.multiply(1.5f);
        moveEntityOn2(vector, livingEntity);
    }

    public void moveEntityOn2(Vec3d vector, LivingEntity livingEntity){
        livingEntity.addVelocity(vector.getX(), vector.getY(), vector.getZ());
    }

    public abstract Identifier getNextCycleBeltType();
}
